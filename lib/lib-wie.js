var lib = module.exports = function(app, knex) {

   const MError = app.MError;

   const webhostingLib = require("models/webhosting.js")
   const webhosting = webhostingLib(knex, app);

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.GetInfoAboutWebhostingById = function(wh_id) {
      return Promise.resolve({wh_id: wh_id});
   }

   wie.BackupWh = function(wh, req) {

      var allUserGroups = [];
      var allUsers;

      var re = {};
      return webhosting.GetWebhostingById(wh.wh_id, true)
        .then(wh=>{

           Array("wh_id", "extras", "hooks").forEach(x=>{
              delete wh[x];
           })
           re.webstore = wh;

           return re;
        })
   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knex.transaction(function(trx){
          var wieTrx = require("Wie").transformWie(app, lib(app, trx));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){
      return knex.raw("SELECT DISTINCT wh_id FROM webhostings")
        .then(rows=>{
            return rows.map(x => x.wh_id);
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      in_data.webstore.wh_id = wh.wh_id;
      in_data.webstore.template_override = JSON.parse(in_data.webstore.wh_template_override);

      // we do it this way, so different wh_storage should not break the process
      return webhosting.GetWebhostingById(wh.wh_id, true)
        .then(whObj=>{
            // it is already present, now need to be adjusted
           return webhosting.CloneSettings(in_data.webstore);
        })
        .catch(ex=>{
           if(ex.message != "WEBHOSTING_NOT_FOUND")
             throw ex;

           return webhosting.Insert(in_data.webstore)
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "restore-wh-databases", e_other: wh.wh_id})
        })


   }



   return wie;

}
