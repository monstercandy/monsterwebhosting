(function(){

/*
// moment(Number);
this is what we need to parse:

p16680 www.emeljfelemlek.hu 127.0.0.1 [14/Oct/2017:03:31:22 +0200] GET /index.php?tmpl=component&phocaslideshow=0&Itemid=1 32452 52.28 5.81 58.09 13056 172.144 200 /emeljfelemlek.hu/pages/index.php 66.249.66.26 /component/phocagallery/8-emlekek-es-emlekhelyek-jeles-pedagogusok-gyor-moson-sopron-megyeben/detail/130-tschida-jnos-srja-s-emlktblja-mriaklnok?tmpl=component&phocaslideshow=0&Itemid=1
p16420 femforgacs.hu 127.0.0.1 [14/Oct/2017:03:12:23 +0200] GET /index.php?op=album&id=11676&t=Cradle_of_Filth_The_Black_Goddess_Rises 29456 0.00 0.00 0.00 2304 21.186 200 /femforgacs.hu/pages/index.php 163.172.68.238

    if(/^(?:www\.)?([^ ]+) .+ (?:(GET|POST|HEAD|PUT|REPORT|OPTIONS|PROPFIND|MKCAL) .+) \d+ (\d+\.\d+) (\d+\.\d+) \d+\.\d+ (\d+) (\d+\.\d+) \d+/) {#
      my ($host, $method, $user, $system,  $mempeak, $time) = ($1,$2,$3,$4,$5,$6);
      # print "H:$host, M:$method, U:$user, $system,  $mempeak, $time, $tarhelyid\n";
      if(($method !~ /^(GET|POST|HEAD)$/)&&($host !~ /^(carddav|caldav)\.monstermedia\.hu$/)) {
         print "INVALID METHOD: $_\n";
      }
      $d{$tarhelyid}{$host}{"hits"}++;
      $d{$tarhelyid}{$host}{"user"}+= $user;
      $d{$tarhelyid}{$host}{"system"}+= $system;
      $d{$tarhelyid}{$host}{"mempeak"}+= $mempeak;
      $d{$tarhelyid}{$host}{"time"}+= $time;


*/

    const lineRegexp = /^p([0-9]+) (?:www\.)?([^ ]+) .+ (?:(?:GET|POST|HEAD|PUT|REPORT|OPTIONS|PROPFIND|MKCAL) .+) [0-9]+ ([0-9]+\.[0-9]+) ([0-9]+\.[0-9]+) [0-9]+\.[0-9]+ ([0-9]+) ([0-9]+\.[0-9]+) [0-9]+/;

    var lib = module.exports = {};
    lib.processLine = function(line){
      var m = lineRegexp.exec(line);
      if(!m) return;
      var re = {};
      re.al_webhosting = m[1];
      re.al_vhost = m[2];
      re.al_stat_user = parseFloat(m[3]);
      re.al_stat_system = parseFloat(m[4]);
      re.al_stat_mempeak = parseFloat(m[5]);
      re.al_stat_servtime = parseFloat(m[6]);
      return re;

    }

})()
