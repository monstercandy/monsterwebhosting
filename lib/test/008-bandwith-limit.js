require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const path = require("path")

    var common = require("./000-common.js")(app, mapi, assert)
    common.doMockQueryTemplate()


 describe("bandwidth limit test", function(){


    var wh = {
                "wh_id": 16800,
                "wh_name": "for tallies",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
             }

    const bandwidthStuff= {24: 100};

    common.createWebhosting('creating a webhosting for this test group', wh)

    it("changing the bandwidth limit", function(done){

          mapi.post( "/"+wh.wh_id, {template_override: {t_bandwidth_limit:bandwidthStuff}}, function(err, result, httpResponse){
                   // console.log("here",err, result)
                    assert.isNull(err)
                    assert.equal(result, "ok")

                    done()

          })


    })


    it('the limit should be returned', function(done) {

         mapi.get( "/"+wh.wh_id,function(err, result, httpResponse){
            // console.log("here",err, result)

            assert.deepEqual(result.template.t_bandwidth_limit, bandwidthStuff)

            done()

         })

    })

    it('getting all webhostings with a bandwidth limit configured', function(done) {

         mapi.get( "/bandwidth-limited",function(err, result, httpResponse){
            // console.log("here",err, result)

            var expected = {};
            expected[wh.wh_id] = bandwidthStuff;
            assert.deepEqual(result, expected);

            done()

         })

    })

 })



}, 10000)

