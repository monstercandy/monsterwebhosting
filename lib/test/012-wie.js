require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("./000-common.js")(app, mapi, assert)

    const storage_id = 32345

    var latestBackup;


    const expectedBackup = { webstore:
   { wh_name: 'wie!',
     wh_user_id: "2345",
     wh_storage: '/web/w3/',
     wh_secretcode: 'abcdef0123456789',
     wh_template: 'WEB10000',
     wh_template_override: '{"t_cron_max_entries":11}',
     wh_php_version: '',
     wh_is_expired: 0,
     wh_is_readonly: 0,
     wh_ftp_accepted_from_latest_admin_ip_only: 0,
     wh_ftp_accepted_from_foreign_ip: 1,
     wh_notify_script_differences: 0,
     wh_notify_foreign_ftp_login_events: 1,
     wh_tally_web_storage_mb: 0,
     wh_tally_mail_storage_mb: 0,
     wh_tally_db_storage_mb: 0,
     wh_max_execution_second: 123,
     wh_php_fpm_conf_extra_lines: 'a: hello\nb: world\n',
     wh_tally_sum_storage_mb: 0,
     wh_tally_sum_storage_mb_last: 0,
     wh_tally_web_storage: {},
     wh_tally_mail_storage: {},
     wh_tally_db_storage: {},
     wh_mail_service_account_password: "foobar",
     wh_installatron_server: 'inst-srv-1' ,
     wh_docker_hints: {},
   } };



   const wieWebhosting = {
                "wh_id": storage_id,
                "wh_name": "wie!",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
                "wh_max_execution_second": 123,
                "wh_php_fpm_conf_extra_lines": "a: hello\nb: world\n",
                "wh_installatron_server": "inst-srv-1",
                "template_override": {t_cron_max_entries: 11},
             }

	describe('basic tests', function() {

        common.setupPhpDistributions();

        common.doMockQueryTemplate();

        common.createWebhosting('creating a stuff with wie', wieWebhosting, storage_id);

        it("chaning wh_mail_service_account_password", function(){
           return mapi.postAsync("/"+storage_id, {wh_mail_service_account_password: "foobar"})
             .then(r=>{
                assert.equal(r.result, "ok");
             })
        })

	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

      cleanup();

        it('webstore should be missing', function(done) {

             mapi.get( "/"+storage_id,function(err, result, httpResponse){
                assert.propertyVal(err, "message", "WEBHOSTING_NOT_FOUND")
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){
                console.log("call returned", result)

                assert.equal(result, "ok");

                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){
             mapi.delete( "/"+storage_id, {}, function(err, result, httpResponse){

                assert.property(result, "id")

                // lame but effective solution to wait until the webhosting is finished
                setTimeout(done, 1000);

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                // console.log(result);

                latestBackup = simpleCloneObject(result);

                var actual = result;
                   Array("updated_at", "created_at").forEach(x=>{
                      assert.ok(actual.webstore[x]);
                      delete actual.webstore[x];
                   })

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

