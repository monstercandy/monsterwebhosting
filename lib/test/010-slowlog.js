require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const fs = require("MonsterDotq").fs();

    var common = require("./000-common.js")(app, mapi, assert)

	describe('inserting an event', function() {

         const slowLib = require("../lib-slowlog.js");

            const slowfn = slowLib.getSlowlogFilename(app.config.get("php_distributions")[0]);


        common.createWebhosting("creating webhosting for slowlog tests", {
                "wh_id": 11091,
                "wh_name": "hello world!",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
             });

        it('list of slowlog events should be empty', function(done) {

             mapi.post( "/slowlog/all/search", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                 assert.property(result, "events")
                assert.ok(Array.isArray(result.events))
                assert.equal(result.events.length, 0);
                done()

             })

        })



        it('getting an event parsed', function() {


            return fs.writeFileAsync(slowfn, "")
              .then(()=>{

                   return mapi.postAsync( "/slowlog/all/reopen", {})

              })
              .then(()=>{

                   return fs.writeFileAsync(slowfn, common.primary_slowlog_test);

              })
              .then(()=>{
                  return new Promise((resolve)=>{
                     return setTimeout(resolve, 1500);
                  })
              })
              .then(()=>{
                  return mapi.postAsync( "/slowlog/all/search", {})

              })
              .then((re)=>{
                 // console.log(re.result.events);
                 assert.ok(re.result.events[0].sl_timestamp);
                 delete re.result.events[0].sl_timestamp;
                 assert.deepEqual(re.result.events, [ { sl_id: 1,
    sl_user_id: '2345',
    sl_webhosting: 11091,
    sl_appserver: 'php/7.0',
    sl_path: '/bm.monstermedia.hu/pages/x.php',
    sl_dump: '[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/x.php:2\n',
     } ]);
              })


        })



	})




}, 10000)

