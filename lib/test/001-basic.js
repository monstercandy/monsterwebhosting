require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}

    const basicTemplatePropertiesFromAccountApi = {"t_storage_max_quota_hard_mb": 1000, "t_storage_max_quota_soft_mb": 900, "t_cron_max_entries": 10}

    var common = require("./000-common.js")(app, mapi, assert)
    common.setupPhpDistributions();

	describe('basic tests', function() {


        const secondWebhosting = {
                "wh_name": "hello world! 2",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_template": "WEB10000",
             }
        const firstWebhostingFromDb =  {
                wh_id: 1234,
                wh_user_id: '2345',
                wh_name: 'hello world!',
                wh_storage: '/web/w3/',
                wh_secretcode: 'abcdef0123456789',
                wh_template_override: "{}",
                wh_template: 'WEB10000',
                wh_php_version: '',
                wh_is_expired: 0,
                wh_is_readonly: 0,
                wh_ftp_accepted_from_latest_admin_ip_only: 0,
                wh_ftp_accepted_from_foreign_ip: 1,
                wh_notify_script_differences: 0,
                wh_notify_foreign_ftp_login_events: 1,
                wh_tally_sum_storage_mb: 0,
                wh_tally_sum_storage_mb_last: 0,
                wh_tally_web_storage_mb: 0,
                wh_tally_web_storage: {},
                wh_tally_mail_storage_mb: 0,
                wh_tally_mail_storage: {},
                wh_tally_db_storage_mb: 0,
                wh_tally_db_storage: {},
                wh_max_execution_second: 0,
                wh_php_fpm_conf_extra_lines: "",
                wh_installatron_server: "",
                wh_docker_hints: {},
                extras: {
                     "web_path": "/web/w3/1234-abcdef0123456789",
                   } ,
                template: {}
        }

       const changeDetails = {
                wh_php_version: '7.0',
                wh_is_expired: true,
                wh_is_readonly: true,
                wh_installatron_server: "foobar",
                wh_ftp_accepted_from_latest_admin_ip_only: true,
                wh_ftp_accepted_from_foreign_ip: false,
                wh_notify_script_differences: true,
                wh_notify_foreign_ftp_login_events: false,
                wh_tally_web_storage_mb: 1,
                wh_tally_mail_storage_mb: 2,
                wh_tally_db_storage_mb: 3,
        }

        const afterChangeInDb = { wh_id: 1234,
  wh_user_id: '2345',
  wh_name: 'hello world!',
  wh_storage: '/web/w3/',
  wh_secretcode: 'abcdef0123456789',
  wh_template: 'WEB10000',
  wh_template_override: "{}",
  wh_php_version: '7.0',
  wh_is_expired: 1,
  wh_is_readonly: 1,
  wh_ftp_accepted_from_latest_admin_ip_only: 1,
  wh_ftp_accepted_from_foreign_ip: 0,
  wh_notify_script_differences: 1,
  wh_notify_foreign_ftp_login_events: 0,
  wh_tally_sum_storage_mb: 0,
  wh_tally_sum_storage_mb_last: 0,
  wh_tally_web_storage_mb: 0,
  wh_tally_web_storage: {},
  wh_tally_mail_storage_mb: 0,
  wh_tally_mail_storage: {},
  wh_tally_db_storage_mb: 0,
  wh_tally_db_storage: {},
  wh_max_execution_second: 0,
  wh_php_fpm_conf_extra_lines: "",
  wh_installatron_server: "foobar",
  wh_docker_hints: {},
  extras: { web_path: '/web/w3/1234-abcdef0123456789' } ,
  template: {}
  }


        const afterTemplateChangeInDb1 = { wh_id: 1234,
  wh_user_id: '2345',
  wh_name: 'hello world!',
  wh_storage: '/web/w3/',
  wh_secretcode: 'abcdef0123456789',
  wh_template: 'from_query',
  wh_template_override: "{\"t_storage_max_quota_hard_mb\":1100}",
  wh_php_version: '7.0',
  wh_is_expired: 1,
  wh_is_readonly: 1,
  wh_ftp_accepted_from_latest_admin_ip_only: 1,
  wh_ftp_accepted_from_foreign_ip: 0,
  wh_notify_script_differences: 1,
  wh_notify_foreign_ftp_login_events: 0,
  wh_tally_sum_storage_mb: 0,
  wh_tally_sum_storage_mb_last: 0,
  wh_tally_web_storage_mb: 0,
  wh_tally_web_storage: {},
  wh_tally_mail_storage_mb: 0,
  wh_tally_mail_storage: {},
  wh_tally_db_storage_mb: 0,
  wh_tally_db_storage: {},
  wh_max_execution_second: 0,
  wh_php_fpm_conf_extra_lines: "",
  wh_installatron_server: "foobar",
  wh_docker_hints: {},
  extras: { web_path: '/web/w3/1234-abcdef0123456789'  } ,
  template: {"t_storage_max_quota_hard_mb": 1100, "t_storage_max_quota_soft_mb": 900, "t_cron_max_entries": 10}
  }

        const afterTemplateChangeInDb2 = { wh_id: 1234,
  wh_user_id: '2345',
  wh_name: 'hello world!',
  wh_storage: '/web/w3/',
  wh_secretcode: 'abcdef0123456789',
  wh_template: 'from_query',
  wh_template_override: "{\"t_cron_max_entries\":11}",
  wh_php_version: '7.0',
  wh_is_expired: 1,
  wh_is_readonly: 1,
  wh_ftp_accepted_from_latest_admin_ip_only: 1,
  wh_ftp_accepted_from_foreign_ip: 0,
  wh_notify_script_differences: 1,
  wh_notify_foreign_ftp_login_events: 0,
  wh_tally_sum_storage_mb: 0,
  wh_tally_sum_storage_mb_last: 0,
  wh_tally_web_storage_mb: 0,
  wh_tally_web_storage: {},
  wh_tally_mail_storage_mb: 0,
  wh_tally_mail_storage: {},
  wh_tally_db_storage_mb: 0,
  wh_tally_db_storage: {},
  wh_max_execution_second: 0,
  wh_php_fpm_conf_extra_lines: "",
  wh_installatron_server: "foobar",
  wh_docker_hints: {},
  extras: { web_path: '/web/w3/1234-abcdef0123456789' } ,
  template: {"t_storage_max_quota_hard_mb": 1000, "t_storage_max_quota_soft_mb": 900, "t_cron_max_entries": 11}
  }


   const afterTemplateChangeInDb3 = { wh_id: 1234,
  wh_user_id: '2345',
  wh_name: 'new name',
  wh_storage: '/web/w3/',
  wh_secretcode: 'abcdef0123456789',
  wh_template: 'from_query',
  wh_template_override: "{\"t_cron_max_entries\":11}",
  wh_php_version: '7.0',
  wh_is_expired: 1,
  wh_is_readonly: 1,
  wh_ftp_accepted_from_latest_admin_ip_only: 1,
  wh_ftp_accepted_from_foreign_ip: 0,
  wh_notify_script_differences: 1,
  wh_notify_foreign_ftp_login_events: 0,
  wh_tally_sum_storage_mb: 0,
  wh_tally_sum_storage_mb_last: 0,
  wh_tally_web_storage_mb: 0,
  wh_tally_web_storage: {},
  wh_tally_mail_storage_mb: 0,
  wh_tally_mail_storage: {},
  wh_tally_db_storage_mb: 0,
  wh_tally_db_storage: {},
  wh_max_execution_second: 0,
  wh_php_fpm_conf_extra_lines: "",
  wh_installatron_server: "foobar",
  wh_docker_hints: {},
  extras: { web_path: '/web/w3/1234-abcdef0123456789' } ,
  template: {"t_storage_max_quota_hard_mb": 1000, "t_storage_max_quota_soft_mb": 900, "t_cron_max_entries": 11}
  }



        it('test unhandled promise rejection', function(done) {

             mapi.get( "/unhandled-promise-rejection",function(err, result, httpResponse){
                assert.propertyVal(err, "message", "INTERNAL_ERROR")
                done()

             })

        })

     common.doMockQueryTemplate()


        it('get all webhostings should be empty', function(done) {

             mapi.get( "/",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })


        common.createWebhosting('inserting a webhosting with ID and secret details', null, 1234)


        it('looking up name by id should work', function(done) {

             mapi.post( "/lookup/id", {ids:['1234']}, function(err, result, httpResponse){

                assert.deepEqual({ '1234': {name: 'hello world!', "user": "2345"} }, result)

                done()
             })
        })

        it('second attempt with the same data should fail', function(done) {

             mapi.put( "/", common.firstWebhosting, function(err, result, httpResponse){

                assert.propertyVal(err,"message", "INTERNAL_ERROR")
                done()
             })
        })

        it('get all webhostings should return the just inserted entry', function(done) {

             mapi.get( "/",function(err, result, httpResponse){
               // console.log("here",err, result)

                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)

                delete result[0].created_at
                delete result[0].updated_at

                delete firstWebhostingFromDb.template // template should not be returned in get all mode

                assert.deepEqual(result, [firstWebhostingFromDb])
                done()

             })

        })



        it('changing some of the details (tally details cannot be changed this way!)', function(done) {

             mapi.post( "/1234", changeDetails, function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.equal(result, "ok")
                done()
             })
        })

        it('get webhosting details by id (tally should be the same)', function(done) {

             mapi.get( "/1234",function(err, result, httpResponse){
                // console.log("here",err, result)

                delete result.created_at
                delete result.updated_at

                assert.deepEqual(result, afterChangeInDb)
                done()

             })

        })

        it('get webhosting by non-existing id should return the id', function(done) {

             var non_existing = "881234";
             mapi.get( "/"+non_existing,function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.propertyVal(err, "message", "WEBHOSTING_NOT_FOUND")
                assert.deepEqual(err.errorParameters, {wh_id: non_existing});

                done()

             })

        })

        it('inserting a webhosting without ID and secret', function(done) {

             secondWebhosting.no_mkdir = true
             mapi.put( "/", secondWebhosting, function(err, result, httpResponse){

                assert.propertyVal(result, "wh_id", 11001)
                done()
             })
        })




        it('overriding some of the details of the template', function(done) {

            var calls = 0
            app.templateLib.QueryTemplate = function(name) {
                calls++
                if(calls == 1)
                  assert.equal(name, "WEB10000")
                if(calls == 2)
                  assert.equal(name, "from_query")
                return Promise.resolve(basicTemplatePropertiesFromAccountApi)
            }

             mapi.post( "/1234", {wh_template:"from_query", template_override: {t_storage_max_quota_hard_mb: 1100, t_storage_max_quota_soft_mb: 900}},
               function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.equal(calls, 2)
                assert.equal(result, "ok")
                done()
             })
        })

        it('get webhosting details by id, wh_template_override and template should be ok', function(done) {

             mapi.get( "/1234",function(err, result, httpResponse){
                // console.log("here",err, result)

                delete result.created_at
                delete result.updated_at

                assert.deepEqual(result, afterTemplateChangeInDb1)
                done()

             })

        })

        it('overriding some additional details of the template', function(done) {

            var calls = 0
            app.templateLib.QueryTemplate = function(name) {
                calls++
                if(calls == 1)
                  assert.equal(name, "from_query")
                if(calls == 2)
                  assert.equal(name, "from_query")
                // console.log(name, calls)
                return Promise.resolve(basicTemplatePropertiesFromAccountApi)
            }

             mapi.post( "/1234", {wh_template:"from_query", template_override: {t_cron_max_entries: 11}},
               function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.equal(calls, 2)
                assert.equal(result, "ok")
                done()
             })
        })

        it('wh_template_override should reflect the new override only (t_cron_max_entries)!', function(done) {

             mapi.get( "/1234",function(err, result, httpResponse){
                // console.log("here",err, result)

                delete result.created_at
                delete result.updated_at

                assert.deepEqual(result, afterTemplateChangeInDb2)
                done()

             })

        })



        it('changing the name of the webhosting', function(done) {

            var calls = 0
            app.templateLib.QueryTemplate = function(name) {
                calls++
                assert.equal(name, "from_query")
                // console.log(name, calls)
                return Promise.resolve(basicTemplatePropertiesFromAccountApi)
            }

             mapi.post( "/1234", {wh_name:"new name"},
               function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.equal(calls, 1)
                assert.equal(result, "ok")
                done()
             })
        })

        it('wh_template_override should not be changed', function(done) {

             mapi.get( "/1234",function(err, result, httpResponse){
                // console.log("here",err, result)

                delete result.created_at
                delete result.updated_at

                assert.deepEqual(result, afterTemplateChangeInDb3)
                done()

             })

        })

        it('overriding some additional details of the template, to a zero value', function(done) {

            var calls = 0
            app.templateLib.QueryTemplate = function(name) {
                calls++
                if(calls == 1)
                  assert.equal(name, "from_query")
                if(calls == 2)
                  assert.equal(name, "from_query")
                // console.log(name, calls)
                return Promise.resolve(basicTemplatePropertiesFromAccountApi)
            }

             mapi.post( "/1234", {wh_template:"from_query", template_override: {t_cron_max_entries: 11,t_storage_max_quota_soft_mb:0}},
               function(err, result, httpResponse){
                // console.log("here",err, result)

                assert.equal(calls, 2)
                assert.equal(result, "ok")
                done()
             })
        })

        it('t_storage_max_quota_soft_mb should be there with zero', function(done) {

             mapi.get( "/1234",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.property(result, "template")
                assert.deepEqual(result.template, {"t_storage_max_quota_hard_mb": 1000, "t_storage_max_quota_soft_mb": 0, "t_cron_max_entries": 11})
                done()

             })

        })


        it('deleting a webhosting, invalid id should be rejected', function(done) {

             mapi.delete( "/123456789", {no_rmdir: true}, function(err, result, httpResponse){

                assert.propertyVal(err,"message", "WEBHOSTING_NOT_FOUND")
                done()
             })
        })


        it('deleting a webhosting, should be ok', function(done) {

             mapi.delete( "/1234", {no_rmdir: true}, function(err, result, httpResponse){
                // console.log("!!!" , err, result)
                assert.property(result, "id")
                done()
             })
        })

	})



}, 10000)

