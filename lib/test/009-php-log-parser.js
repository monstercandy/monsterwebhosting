require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("./000-common.js")(app, mapi, assert)


    const test1 = common.primary_slowlog_test;

   const expected1 = {
  sl_webhosting: '11091',
  sl_path: '/bm.monstermedia.hu/pages/x.php',
  sl_dump: '[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/x.php:2\n' };


     const parserLib = require("../lib-php-slowlog-parser.js")

     describe("parsing stuff", function(){

         it("supplying the stuff at once", function(done){

            var parser = parserLib({
                onEvent: function(event){
                    // console.log(event);
                    validResponse(event, expected1, done);

                }
            });
            parser.processLine(test1);


         })

         it("supplying the stuff line by line", function(done){

            var parser = parserLib({
                onEvent: function(event){
                    //console.log(event);
                    validResponse(event, expected1, done);
                }
            })
            var testLines = test1.split("\n");
            testLines.forEach(line=>{
               parser.processLine(line);

            })

         })




     })

     function validResponse(event, expected, done) {
        assert.property(event, "sl_timestamp");
        assert.ok(event.sl_timestamp);
        delete event.sl_timestamp;
        assert.deepEqual(event, expected);
        done();
     }


}, 10000)

