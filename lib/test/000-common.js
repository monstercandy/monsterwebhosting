var lib = module.exports = function(app, mapi, assert) {

	var re = {

        setupPhpDistributions: function(){
        	  const path = require("path");
	          const os = require("os")
	          if(!app.config.get("o_php_distributions")) {
	          	 app.config.set("o_php_distributions", app.config.get("php_distributions"))
	          }

	          var re = {}
	          re.php_fpm_conf_file = path.join(os.tmpdir(), "mocked-php-fpm.conf")
	          re.o_php_distributions = app.config.get("o_php_distributions")

	          app.config.set("php_distributions", [{"php_version":"7.0", "slowlog_path":"phpslow.txt", "basedir": "vol.d", "conf_file": re.php_fpm_conf_file}]) // conf_file is for testing only

	          return re
	    },


		primary_slowlog_test: `
[13-Oct-2017 18:10:33]  [pool p11091] pid 8216
script_filename = /bm.monstermedia.hu/pages/x.php
[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/x.php:2
`,

       doMockQueryTemplate: function(){

	       app.templateLib = {
	          QueryTemplate: function(name) {
	             return Promise.resolve({})
	          }
	       }

       },
	   firstWebhosting: {
                "wh_id": 1234,
                "wh_name": "hello world!",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
             },
        mockedAccountInfo: function(){
        	var re = {
        		       getInfoCalled: false,
		               GetInfo: function(user_id) {
		                  re.getInfoCalled = true
		                  return Promise.resolve("everything fine")
		               }
		             }
		    return re
        },
        createWebhosting: function(testname, webhosting, expected_id){
        	 if(!webhosting) webhosting = re.firstWebhosting

		      it(testname, function(done) {

		             app.MonsterInfoAccount = re.mockedAccountInfo()

		             webhosting.no_mkdir = true

		             mapi.put( "/", webhosting, function(err, result, httpResponse){
		             	if(err)console.log(err)
		             	assert.isNull(err)
		                assert.ok(app.MonsterInfoAccount.getInfoCalled)
		                if(expected_id)
		                   assert.propertyVal(result, "wh_id", expected_id)
                        // assert.property(result, "task_id")
		                if(!webhosting.wh_id) webhosting.wh_id = result.wh_id
		                done()
		             })
		        })

        }
    }

    return re
}
