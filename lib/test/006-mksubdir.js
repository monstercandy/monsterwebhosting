require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

	describe('mksubdir', function() {

      const mksubdir = require("mksubdir.js")


        it('should create the dirs and chown them', function() {
           var mkdired = 0
           var chowned = 0

           var fs = {
              mkdirAsync: function(path){
                 mkdired++
                 if(mkdired == 1)
                   assert.equal(path, "basedir/")
                 else
                 if(mkdired == 2)
                   assert.equal(path, "basedir/subdir1")
                 else
                 if(mkdired == 3)
                   assert.equal(path, "basedir/subdir2")
                 else
                 if(mkdired == 4)
                   assert.equal(path, "basedir/subdir3")
                 else
                 if(mkdired == 5)
                   assert.equal(path, "basedir/subdir4")
                 else
                   throw new Error("unexpected mkdir"+path)

                 var e = new Error()
                 e.code = "EEXIST"
                 return Promise.reject(e)
              },
              chownAsync: function(path, uid, gid) {
                 chowned++
                 if(chowned == 1){
                   assert.equal(path, "basedir/")
                   assert.equal(uid, 12345)
                   assert.equal(gid, 65534)
                 }
                 else
                 if(chowned == 2){
                   assert.equal(path, "basedir/subdir1")
                   assert.equal(uid, 10000)
                   assert.equal(gid, 65534)
                 }
                 else
                 if(chowned == 3){
                   assert.equal(path, "basedir/subdir2")
                   assert.equal(uid, 10000)
                   assert.equal(gid, 65534)
                 }
                 else
                 if(chowned == 4){
                   assert.equal(path, "basedir/subdir3")
                   assert.equal(uid, 10001)
                   assert.equal(gid, 65534)
                 }
                 else
                 if(chowned == 5){
                   assert.equal(path, "basedir/subdir4")
                   assert.equal(uid, 12345)
                   assert.equal(gid, 65534)
                 }
                 else
                   throw new Error("unexpected chown")

                 return Promise.resolve()
              }


           }

           return mksubdir(
              "basedir", 
              [
                 {"12345":"/"}, 
                 {"10000":["subdir1", "subdir2"], "10001": "subdir3"}, 
                 null,
                 {"first": "subdir4"}
              ], 65534, fs)
             .then(()=>{
                assert.equal(mkdired, 5)
                assert.equal(mkdired, chowned)

             })

        })


	})




}, 10000)

