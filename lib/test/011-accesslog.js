require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const fs = require("MonsterDotq").fs();

    var common = require("./000-common.js")(app, mapi, assert)

    const parserLib = require("../lib-php-accesslog-parser.js");

    const line1 = "p16680 www.emeljfelemlek.hu 127.0.0.1 [14/Oct/2017:03:31:22 +0200] GET /index.php?tmpl=component&phocaslideshow=0&Itemid=1 32452 52.28 5.81 58.09 13056 172.144 200 /emeljfelemlek.hu/pages/index.php 66.249.66.26 /component/phocagallery/8-emlekek-es-emlekhelyek-jeles-pedagogusok-gyor-moson-sopron-megyeben/detail/130-tschida-jnos-srja-s-emlktblja-mriaklnok?tmpl=component&phocaslideshow=0&Itemid=1";
    const line2 = "p16420 femforgacs.hu 127.0.0.1 [14/Oct/2017:03:12:23 +0200] GET /index.php?op=album&id=11676&t=Cradle_of_Filth_The_Black_Goddess_Rises 29456 0.00 0.00 0.00 2304 21.186 200 /femforgacs.hu/pages/index.php 163.172.68.238";

	describe('parsing', function() {
    it("parsing line #1", function(){
       var r = parserLib.processLine(line1);
       assert.deepEqual(r, { al_webhosting: '16680',
  al_vhost: 'emeljfelemlek.hu',
  al_stat_user: 52.28,
  al_stat_system: 5.81,
  al_stat_mempeak: 13056,
  al_stat_servtime: 172.144 });


    });

    it("parsing line #2", function(){
       var r = parserLib.processLine(line2);
       assert.deepEqual(r, { al_webhosting: '16420',
  al_vhost: 'femforgacs.hu',
  al_stat_user: 0,
  al_stat_system: 0,
  al_stat_mempeak: 2304,
  al_stat_servtime: 21.186 });


    });

  });


  describe('processing accesslog file', function() {
    const accesslogLib = require("../lib-accesslog.js");
    const slowfn = accesslogLib.getAccesslogFilename(app.config.get("php_distributions")[0]);

    it("preparing the file", function(){

        const fs = require("fs");
        fs.writeFileSync(slowfn, line1+"\n"+line2+"\n"+line1+"\n"+line2+"\n");

    });

    it("triggering the process manually", function(done){
             mapi.post( "/accesslog/all/process", {}, function(err, result, httpResponse){
                 // console.log("here",err, result)
                 assert.deepEqual(result, { '16420':
   { 'femforgacs.hu':
      { al_stat_user: 0.00,
        al_stat_system: 0.00,
        al_stat_mempeak: 4608.00,
        al_stat_servtime: 42.37,
        al_stat_hits: 2,
        al_vhost: 'femforgacs.hu',
        al_webhosting: '16420' } },
  '16680':
   { 'emeljfelemlek.hu':
      { al_stat_user: 104.56,
        al_stat_system: 11.62,
        al_stat_mempeak: 26112.00,
        al_stat_servtime: 344.29,
        al_stat_hits: 2,
        al_vhost: 'emeljfelemlek.hu',
        al_webhosting: '16680' } } });
                 done()

             })

    });

    it("and querying the db should return these stuffs", function(done){
             mapi.post( "/accesslog/all/search", {}, function(err, result, httpResponse){
                 // console.log("here",err, result)
                 result.events.forEach(line=>{
                    assert.ok(line.al_timestamp);
                    delete line.al_timestamp;
                 })
                 assert.deepEqual(result,{ events:
   [ { al_id: 1,
       al_webhosting: 16420,
       al_vhost: 'femforgacs.hu',
       al_stat_user: 0,
       al_stat_system: 0,
       al_stat_hits: 2,
       al_stat_mempeak: 4608,
       al_stat_servtime: 42.37
     },
     { al_id: 2,
       al_webhosting: 16680,
       al_vhost: 'emeljfelemlek.hu',
       al_stat_user: 104.56,
       al_stat_system: 11.62,
       al_stat_hits: 2,
       al_stat_mempeak: 26112,
       al_stat_servtime: 344.29
     } ] });
                 done()

             })

    });

  });


}, 10000)

