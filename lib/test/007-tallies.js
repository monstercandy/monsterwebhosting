require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const path = require("path")

    var common = require("./000-common.js")(app, mapi, assert)
    common.doMockQueryTemplate()


 describe("tally tests", function(){


    var wh = {
                "wh_id": 16500,
                "wh_name": "for tallies",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
             }

    common.createWebhosting('creating a webhosting for this test group', wh)

    it("changing the numbers only", function(done){

          mapi.post( "/"+wh.wh_id+"/tallies", {wh_tally_web_storage_mb:1.001101, wh_tally_mail_storage_mb: 2, wh_tally_db_storage_mb: 3}, function(err, result, httpResponse){
                   // console.log("here",err, result)
                    assert.isNull(err)
                    assert.equal(result, "ok")

                    done()

          })


    })


    it('get webhosting details should reflect the tallies', function(done) {

         mapi.get( "/16500",function(err, result, httpResponse){
            // console.log("here",err, result)

            assert.propertyVal(result, "wh_tally_web_storage_mb", 1)
            assert.propertyVal(result, "wh_tally_mail_storage_mb", 2)
            assert.propertyVal(result, "wh_tally_db_storage_mb", 3)
            assert.propertyVal(result, "wh_tally_sum_storage_mb", 6)

            assert.deepEqual(result.wh_tally_web_storage, {})
            assert.deepEqual(result.wh_tally_mail_storage, {})
            assert.deepEqual(result.wh_tally_db_storage, {})

            done()

         })

    })


   const c_web = {"some-dir1.hu":5,"some-dir2.hu":5}
   const c_email = {"email1@email.hu": 10, "email2@email.hu": 10}
   const c_db = {"some-db1": 15, "some-db2": 15}

    it("changing the details (note the sum is being overridden)", function(done){

          mapi.post( "/"+wh.wh_id+"/tallydetails", {
             wh_tally_web_storage:c_web, 
             wh_tally_web_storage_mb: 1000, 
             wh_tally_mail_storage: c_email, 
             wh_tally_db_storage: c_db
           }, 

            function(err, result, httpResponse){
                   // console.log("here",err, result)
                    assert.isNull(err)
                    assert.equal(result, "ok")

                    done()

          })


    })


    it('get webhosting details should reflect the tallies, details should be there', function(done) {

         mapi.get( "/16500",function(err, result, httpResponse){
            // console.log("here",err, result)

            assert.propertyVal(result, "wh_tally_web_storage_mb", 1000)
            assert.propertyVal(result, "wh_tally_mail_storage_mb", 20)
            assert.propertyVal(result, "wh_tally_db_storage_mb", 30)
            assert.propertyVal(result, "wh_tally_sum_storage_mb", 1050)

            assert.deepEqual(result.wh_tally_web_storage, c_web)
            assert.deepEqual(result.wh_tally_mail_storage, c_email)
            assert.deepEqual(result.wh_tally_db_storage, c_db)

            done()

         })

    })



 })

 describe("walking", function(){
    it('walking a webhosting', function(done) {

        const path = require("path").posix
        // const request = require("RequestCommon")

        app.GetWalker = function() {
            return function(options) {
               assert.equal(options.basedir, "/web/w3/16500-abcdef0123456789")
               return Promise.resolve({dir1: 1});
            }
        }

        const libWebhosting = require("models/webhosting.js")
        const wh = libWebhosting(app.knex, app)

        wh.GetWebhostingById(16500)
          .then(wh=>{
             return wh.Walk()
          })
          .then(result=>{
            // console.log("foo",err, result)
            assert.deepEqual({ wh_tally_web_storage_mb: 1, wh_tally_web_storage: { dir1: 1 } }, result)

            done()
        })
        .catch(done)

    })


 })



}, 10000)

