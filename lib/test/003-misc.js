require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const path = require("path")

    var common = require("./000-common.js")(app, mapi, assert)
    common.doMockQueryTemplate()

 describe("misc", function(){
    it("supported php versions", function(done){

          var php = common.setupPhpDistributions()

             mapi.get( "/supported/php",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.ok(result.length == 1)
                app.config.set("php_distributions",php.o_php_distributions)
                done()

             })

    })

    it("supported python versions", function(done){

             mapi.get( "/supported/python",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.ok(result.length > 0)
                done()

             })

    })

    it("supported storages", function(done){

             mapi.get( "/supported/storages",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.ok(result.length > 0)
                done()

             })

    })
 })


 describe("php-fpm config gen", function(){


    var wh = {
                "wh_id": 16387,
                "wh_name": "hello world!",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
                "wh_max_execution_second": 123,
                "wh_php_fpm_conf_extra_lines": "a: hello\nb: world\n"
             }

    common.createWebhosting('creating a webhosting for this test group', wh)

    it("generate php-fpm config", function(done){
          const fs = require("fs")

          var php = common.setupPhpDistributions()

          fs.unlink(php.php_fpm_conf_file, function(err){
              // we ignore unlink errors here

              mapi.post( "/"+wh.wh_id+"/php/fpm/config", {}, function(err, result, httpResponse){
                    // console.log("here",err, result)
                    assert.isNull(err)
                    assert.equal(result, "ok")

                    fs.readFile(php.php_fpm_conf_file, "utf-8", function(err, data){
                        if(err) return done(err)

                        // console.log(data)

                        assert.ok(data.indexOf("[p"+wh.wh_id+"]") > -1)
                        assert.ok(data.indexOf("listen = 127.0.0.1:17001") > -1)
                        assert.ok(data.indexOf("access.log = ") > -1);
                        assert.ok(data.indexOf("#access.log = ") < 0);
                        assert.ok(data.indexOf("user = "+wh.wh_id) > -1)
                        assert.ok(data.indexOf("chroot = /web/w3/"+wh.wh_id+"-abcdef0123456789") > -1)

                        assert.ok(data.indexOf("\na: hello\n") > -1)
                        assert.ok(data.indexOf("\nb: world\n") > -1)

                        assert.ok(data.indexOf("request_terminate_timeout = 123s") > -1)
                        assert.ok(data.indexOf("php_value[max_execution_time] = 123") > -1)

                        app.config.set("php_distributions",php.o_php_distributions)
                        done()

                    })


              })

          })


    })

    it("read php-fpm config", function(done){

      var php = common.setupPhpDistributions()

              mapi.get( "/"+wh.wh_id+"/php/fpm/config", function(err, result, httpResponse){
                    const fs = require("fs")
                    // console.log("here",err, result)
                    assert.isNull(err)
                    assert.property(result,'vol.d')
                    assert.ok(result['vol.d'].indexOf("[p"+wh.wh_id+"]") > -1)
                    fs.unlinkSync(php.php_fpm_conf_file)
                    app.config.set("php_distributions",php.o_php_distributions)
                    done()

              })

    })


    it("rehash fpms", function(done){
         var php = common.setupPhpDistributions()

         mapi.post( "/php/fpm/rehash", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.equal(result, 0)

                app.config.set("php_distributions",php.o_php_distributions)

                done()
          })
    })



 })


 describe("szar", function(){

    const expectedSzarEnv = { SZAR_chroot: '/web/w3/16387-abcdef0123456789',
     SZAR_backupdir: '/',
     SZAR_subrepo: 's1-16387-web',
     SZAR_user: 16387,
     SZAR_group: 65534 }

    it("jump url should be calcualted", function(done){

         mapi.post( "/16387/szar/jump/web", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNull(err)
                assert.property(result, "url")
                assert.ok(result.url.indexOf("https://") == 0)
                assert.ok(result.url.indexOf("&a=1") > -1)


                done()
          })
    })

    backupWithScriptNotification(false)
    backupWithScriptNotification(true)


    it("checkpoints should be calculated correctly", function(done){

        const taskId= "foobar"

            app.commander = {
                   spawn: function(params){
                      console.log(params)

                      assert.ok(params.stderrReader);
                      delete params.stderrReader;

                      assert.deepEqual(params, { chain:
   { executable: '/some/path/to/szar-backup.pl',
     args: [ '/some/path/to/client-subrepo-web.conf', 'rcheckpoints' ],
  spawnOptions: { env: expectedSzarEnv}
      },
  omitAggregatedOutput: false,
  dontLog_stdout: true,
  executeImmediately: true })


                      return Promise.resolve({id:taskId, executionPromise: Promise.resolve({output: "2017/04/01 12:03:59 [INFO] CP: 20170329010605\n2017/04/01 12:03:59 [INFO] CP: 20170328010604\n"})})
                   }
                }

         mapi.post( "/16387/szar/checkpoints/web", {}, function(err, result, httpResponse){

                // console.log("here",err, result)
                assert.isNull(err)
                assert.deepEqual(result, ["20170329010605","20170328010604"])

                done()
          })
    })

    it("checkpoints should return empty array when there is an error during execution", function(done){

        const taskId= "foobar"

            app.commander = {
                   spawn: function(params){
                      //console.log(params)


                      return Promise.reject(new Error("foobar"))
                   }
                }

         mapi.post( "/16387/szar/checkpoints/web", {}, function(err, result, httpResponse){

                // console.log("here",err, result)
                assert.isNull(err)
                assert.deepEqual(result, [])

                done()
          })
    })

    it("when restoring, szar should be called with the appropriate parameters", function(done){

        const taskId= "foobar"

            app.commander = {
                   spawn: function(params){
                     // console.log(params)

                     assert.property(params,"filter_stdout")
                     delete params.filter_stdout

                      assert.ok(params.stderrReader);
                      delete params.stderrReader;

                      assert.deepEqual(params, { chain:
   { executable: '/some/path/to/szar-backup.pl',
     args:
      [ '/some/path/to/client-subrepo-web.conf',
        'restore',
        '/subdir/',
        '/subdir/',
        '20170101101010' ],
spawnOptions:
   { env:expectedSzarEnv }
         },
  omitAggregatedOutput: true,
  dontLog_stdout: true,
   })


                      return Promise.resolve({id:taskId, executionPromise: Promise.resolve()})
                   }
                }

         mapi.post( "/16387/szar/restore/web", {dir: "/subdir", checkpoint: "20170101101010"}, function(err, result, httpResponse){

                // console.log("here",err, result)
                assert.isNull(err)
                assert.propertyVal(result, "id", taskId)

                done()
          })
    })

    function backupWithScriptNotification(wantScriptDiff) {

            it("backup should invoke the correct szar command: "+wantScriptDiff, function(done){
                const taskId = "some-id"



                var callReturned = false
                var emailSent = false
                var oMailer = app.GetMailer

                if(wantScriptDiff){
                      app.config.set("send_email_notification_about_changed_scripts", true)
                      app.GetMailer = function() {
                        return {
                          SendMailAsync: function(params){
                             console.log(params)
                             assert.property(params, "context")
                             assert.property(params.context, "diffFilename")
                             delete params.context.diffFilename
                             assert.deepEqual(params, { toAccount: { user_id: '2345', emailCategory: 'TECH' },
        context:
         { server: "s1",
           wh_name: 'hello world!',
           allChanges: 1,
           remove_difflogs_older_than_days: app.config.get("remove_difflogs_older_than_days"),
           changedScripts: [ 'some/crappy.php' ],
         },
        template: 'webhosting/diff-log' })

                             emailSent = true
                             checkForDone()
                             return Promise.resolve()
                          }
                        }
                      }
                }

                var commanderCalls = 0

                var ocommander = app.commander
                app.commander = {
                   spawn: function(params){
                      commanderCalls++
                      var commanderArgs = [ '/some/path/to/client-subrepo-web.conf' ]
                      if(commanderCalls == 1)
                        commanderArgs.push('backup')
                      else
                      if(commanderCalls == 2)
                        commanderArgs.push('cleanup')
                      else
                        throw new MError("unexpected commander call")


                       console.log(params)
                      if((wantScriptDiff)&&(commanderCalls == 1)) {
                         assert.property(params,"stdoutReader")
                         params.stdoutReader("2017/03/26 14:23:11 [INFO] NEW F GZ: /some/crappy.php")
                         delete params.stdoutReader
                      }

                      assert.ok(params.stderrReader);
                      delete params.stderrReader;

                      assert.deepEqual(params, { chain:
           { executable: '/some/path/to/szar-backup.pl',
             args: commanderArgs,
          spawnOptions:
           { env: expectedSzarEnv }
              },
          dontLog_stdout: true,
          omitAggregatedOutput: true,

                      })

                      return Promise.resolve({id:taskId, executionPromise: Promise.resolve()})
                   }
                }

                  mapi.post( "/16387", {wh_notify_script_differences:wantScriptDiff}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)

                        doBackup()

                  })


                function doBackup(){
                 mapi.post( "/16387/szar/backup/web", {}, function(err, result, httpResponse){
                        // console.log("here",err, result)
                        assert.isNull(err)
                        assert.deepEqual(result, {id: taskId})

                        callReturned = true
                        checkForDone()
                  })
                }

                function checkForDone(){
                  // console.log("checkfordone", wantScriptDiff, emailSent, callReturned)
                  if((wantScriptDiff)&&(!emailSent)) return
                  if(!callReturned) return

                   assert.equal(commanderCalls, 2);

                   app.commander = ocommander
                   app.GetMailer = oMailer
                   done()
                }
            })
    }

 })

 describe("partitions parsing", function(){
    it("parsing proc/partitions", function(){
        const fs = require("fs");
        var p = fs.readFileSync("test/partitions1.txt", "utf-8");
        const t = require("router-system.js");
        var ps = t.parsePartitions(p);
        assert.deepEqual(ps, ["sdb","sda"]);

    })
 });


}, 10000)

