require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../webhosting-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}

    var common = require("./000-common.js")(app, mapi, assert)
    common.doMockQueryTemplate()

describe("hooks", function(){

    var wh = {
                "wh_id": 16386,
                "wh_name": "hello world!",
                "wh_user_id": 2345,
                "wh_storage": "/web/w3/",
                "wh_secretcode": "abcdef0123456789",
                "wh_template": "WEB10000",
                "wh_max_execution_second": 123,
                "wh_php_fpm_conf_extra_lines": "a: hello\nb: world\n"
             }

        common.createWebhosting('creating a webhosting for this test group', wh)


    it("hook with invalid name should return faliora", function(done){

             mapi.post( "/"+wh.wh_id+"/hook/foobar",{},function(err, result, httpResponse){
                assert.propertyVal(err, "message", "HOOK_NOT_CONFIGURED")
                done()

             })

    })


    Array({
       test:"hook call should result in a commander spawn call",
       url: "/"+wh.wh_id+"/hook/init",
       emitterId: 3000,
       task_ids: [999001],
       expected: {id: 999001}
     },
     {
       test:"hook call for all webhostings should behave the same way",
       task_ids: [999001, 999002],
       url: "/hook/init",       
       expected: {id: 999001}
     }
    ).forEach(test=>{

          it(test.test, function(done){

              var originalHooks = app.config.get("hooks")
              var hooks = app.config.get("hooks")
              hooks.init = {"executable":"some-stuff"}
              app.config.set("hooks", hooks)


              var spawnCalls = 0
              var emitterCalls = 0
              var emitterSpawns = 0


              app.commander = {
                EventEmitter: function(){
                  emitterCalls++
                  return {
                     send_stdout: function(msg){
                        console.log("gemitter stdout", msg)
                     },
                     close: function(code){
                         assert.equal(code || 0, 0)
                         assert.equal(emitterCalls, 1)
                         assert.equal(emitterSpawns, 1)

                         app.config.set("hooks", originalHooks)
                         done()
                     },
                     spawn: function(){
                         emitterSpawns++
                         return Promise.resolve({id: test.expected.id, executionPromise: Promise.reject("noone cares")})
                     },
                  }
                },
                spawn: function(command, template) {
                    assert.property(command, "emitter")
                    delete command.emitter
                    assert.deepEqual(command, {omitAggregatedOutput: true, chain:hooks.init})
                    // console.log("!!! spawn", command)
                    assert.property(template, "wh_id")
                    assert.property(template, "wh_secretcode")
                    assert.property(template, "extras")
                    assert.property(template.extras, "web_path")
                    return Promise.resolve({id: test.task_ids[spawnCalls++]})
                }
              }

                 mapi.post( test.url,{},function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(spawnCalls, test.task_ids.length)
                    assert.deepEqual(result, test.expected)


                 })

          })

    })


    it("before and after hooks should be called as well", function(done){

              var originalHooks = app.config.get("hooks")
              var hooks = app.config.get("hooks")
              hooks["init-before"] = {"executable":"some-init-stuff-before"}
              hooks["init-after"] = {"executable":"some-init-stuff-after"}
              app.config.set("hooks", hooks)

              const emitterId = 99000


              var spawnCalls = 0
              var emitterCalls = 0
              var emitterSpawns = 0

              app.commander = {
                EventEmitter: function(){
                  emitterCalls++
                  return {
                     send_stdout: function(msg){
                        console.log("gemitter stdout", msg)
                     },
                     close: function(code){
                         assert.equal(code || 0, 0)
                         assert.equal(emitterCalls, 1)
                         assert.equal(emitterSpawns, 1)

                         app.config.set("hooks", originalHooks)
                         done()
                     },
                     spawn: function(){
                         emitterSpawns++
                         return Promise.resolve({id: emitterId, executionPromise: Promise.reject("noone cares")})
                     },
                  }
                },

                spawn: function(command, template) {
                    assert.property(command, "emitter")
                    delete command.emitter

                    // console.log("!", command, {omitAggregatedOutput: true, chain:hooks["init-before"]}, spawnCalls)

                    if(spawnCalls== 0)
                      assert.deepEqual(command, {omitAggregatedOutput: true, chain:hooks["init-before"]})
                    if(spawnCalls== 1)
                      assert.deepEqual(command, {omitAggregatedOutput: true, chain:hooks["init-after"]})
                    assert.property(template, "wh_id")
                    assert.property(template, "wh_secretcode")
                    assert.property(template, "extras")
                    assert.property(template.extras, "web_path")
                    return Promise.resolve({id: spawnCalls++})
                }
              }

                 mapi.post( "/"+wh.wh_id+"/hook/init",{},function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(spawnCalls, 2)
                    assert.deepEqual(result, {id:emitterId})

                 })

          })


})




}, 10000)

