module.exports = function(app) {

    var router = app.ExpressPromiseRouter({mergeParams: true});


  router.use(function(req,res,next){
     if(!req.body.json) req.body.json = {};
     if(req.params.webhosting)
        req.body.json.al_webhosting = req.params.webhosting;
     next();
  });


  router.post("/search", function(req,res,next){
          return req.sendPromResultAsIs(
              app.getAccesslog().Query(req.body.json)
          )

     })

  router.post("/count", function(req,res,next){
          return req.sendPromResultAsIs(
             app.getAccesslog().Count(req.body.json)
          )
     })

  router.post("/process", function(req,res,next){
          return req.sendPromResultAsIs(
             app.getAccesslog().process()
          )
     })

  return router





}
