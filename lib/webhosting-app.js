//permissions needed: ["EMIT_LOG","INFO_ACCOUNT","INFO_WEBHOSTING_TEMPLATE","ADMIN_TALLY","SEND_EMAIL_TO_USER"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "webhosting"

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');
    const MError = me.Error


    config.defaults({
      "cleanup": {
          "45 5 * * *": [
            {table: "slowlog", agePrefix: "sl_", ageDays: 90},
            {table: "accesslog", agePrefix: "al_", ageDays: 30},
          ]
       },
      "php_fpm_accesslog_disabled": false,
      "deprecatedPhpVersions": ["5.3", "5.4", "5.5", "5.6"],

      "cron_php_accesslog_process": "50 4 * * *",

      "slowlog_parser": false,
      "rotate_slowlog_after_hours": 24,
      "slowlog_sync_flush_seconds": 10,

      "execute_init_hooks_on_startup": false,

      "legacy_php_fpm": false,

      "python_legacy_support": false,

      "remove_difflogs_older_than_days": 21,
      "send_email_notification_about_changed_scripts": true,
      "max_changed_scripts_in_email": 50,
      "tally_warning_threshold_multiplier": 0.9,
      "walk_skip_directories": [],
      "webhostingtemplates_info_directory": "memory",
      "cache_webhostingtemplates_info_seconds": 3600,
      "cleanup_webhostingtemplates_info_ms": 10*60*1000, // 10 minutes
      "python_max_req_lines": 100,
      "php_distributions": [],
      "python_major_versions":[],
      "reload_phpfpm_after_webhosting_create": true,
      "reload_phpfpm_after_webhosting_delete": false,
      "python_requirement_command": "/opt/MonsterWebhosting/lib/cmds/requirements.sh",
    });

    const deprecatedPhpVersions = config.get("deprecatedPhpVersions");


    config.appRestPrefix = "/webhosting"

    var app = me.Express(config, moduleOptions);
    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var common = require("lib-common.js")(app)

    app.SzarLib = require("lib-mc-szar.js")(app)


    app.storageCategories = ["web","mail"]
    processStorages()

    var router = app.PromiseRouter(config.appRestPrefix)

    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)


    var wieRef = {};
    const wieLib = require("Wie");
    var wieRouter = wieLib.getSubrouter(app, wieRef)
    router.use("/wie", wieRouter);

    require("router-wie-extra.js")(app, wieRouter);


    app.Tail = require('tail').Tail;


    const MonsterInfoLib = require("MonsterInfo")
    app.MonsterInfoWebhostingtemplates = MonsterInfoLib.webhostingtemplates({config: app.config, accountServerRouter: app.ServersRelayer})
    app.MonsterInfoAccount = MonsterInfoLib.account({config: app.config, accountServerRouter: app.ServersRelayer})

    app.templateLib = require("lib-template.js")(app)

    router.get("/unhandled-promise-rejection", function(req,res,next){
        return Promise.resolve()
          .then(()=>{
              throw new Error("TEST");
          })
    })

   // this is the first one purposefully
   require("router-misc.js")(app, router, common)

   router.use("/mc", require("router-mc.js")(app));

   router.use("/system", require("router-system.js")(app));

   var slowlogRouter = require("webhosting-slowlog-router.js")(app);
   router.use("/slowlog/all", slowlogRouter);
   router.use("/slowlog/by-webhosting/:webhosting", slowlogRouter);

   var accesslogRouter = require("webhosting-accesslog-router.js")(app);
   router.use("/accesslog/all", accesslogRouter);
   router.use("/accesslog/by-webhosting/:webhosting", accesslogRouter);

   if(app.config.get("python_legacy_support")) {
      app.pythonLib = require("router-python.js")(app, router, common)    
   }


   require("router-hooks.js")(app, router, common)

   if(app.config.get("legacy_php_fpm")) {
      require("router-php-fpm.js")(app, router, common)    
   }

   // require("router-webhosting.js")(app, router, common)

   require("router-webhosting.js")(app, router, common)


    app.GetFtpMapiPool = app.GetRelayerMapiPool;

   app.GetWalker = function(root, options) {
      const walker = require('WalkerDu');
      return walker;
   }

   app.getSupportedPythonMajorVersions = function(){
      return app.config.get("python_major_versions")
   }

    app.Prepare = function() {
       app.knex = require("MonsterKnex")(config)

       wieRef.wie = require("lib-wie.js")(app, app.knex);
       wieLib.transformWie(app, wieRef.wie);

       app.WebhostingLib = require("models/webhosting.js")

       return app.knex.migrate.latest()
         .then(()=>{
            app.getSlowlog() // this gonna initialize the slowlog parsers
            app.getAccesslog();
         })
         .then(()=>{
            if(!app.config.get("execute_init_hooks_on_startup")) return;

            return app.WebhostingLib(app.knex, app).GetAllWebhostings()
              .then(webhostings=>{
                  return common.runHooks(webhostings, "init", {});
              })

         })
    }

    app.Cleanup = function() {

       var fn = config.get("db").connection.filename
       if(!fn) return Promise.resolve()

       return fs.unlinkAsync(fn).catch(x=>{}) // the database might not exists, which is not an error in this case
         .then(()=>{
            const slowlogLib = require("lib-slowlog.js");
            return fs.unlinkAsync(slowlogLib.getSlowlogFilename(app.config.get("php_distributions")[0])).catch(x=>{});
         })
         .then(()=>{
            const accesslogLib = require("lib-accesslog.js");
            return fs.unlinkAsync(accesslogLib.getAccesslogFilename(app.config.get("php_distributions")[0])).catch(x=>{});
         })
    }

    app.getSlowlog=function(){
      const slowlog = require("lib-slowlog.js")(app, app.knex);
      return slowlog;
    }
    app.getAccesslog=function(){
      const accesslog = require("lib-accesslog.js")(app, app.knex);
      return accesslog;
    }

    const cron = require('Croner');

    var csp = app.config.get("cron_storage_walking_for_tallies")
    if(csp) {
        cron.schedule(csp, function(){
            return walkTallies()
        });
    }

    csp = app.config.get("cron_szar_backup")
    if(csp) {
        cron.schedule(csp, function(){
            return szarBackupAll()
        });
    }

    csp = app.config.get("cron_process_all")
    if(csp) {
        cron.schedule(csp, function(){
            return walkTallies()
              .catch(ex=>{console.error("error during tally walk", ex)})
              .then(()=>{
                 return szarBackupAll()
              })
              .catch(ex=>{
                console.error("error during szar backup", ex)
              })

        });
    }

    csp = app.config.get("cron_tally_warning_emails")
    if(csp) {
        cron.schedule(csp, function(){
            return getWh().SendTallyWarnings()
        });
    }

    app.getPhpVersions = function(withEmpty){
           var re = app.config.get("php_distributions")
           re = re.map(function(x){
              return x.php_version
           })
           if(withEmpty)
             re.push("");
           return re;
    }

    app.getPhpVersionsArr = function(){

           //, [{"php_version":"7.0", "basedir": "/usr/local/php70", "conf_file": re.php_fpm_conf_file}]) // conf_file is for testing only
           var re = app.config.get("php_distributions")
           re = re.map(function(x){
              return { version: x.php_version, deprecated: deprecatedPhpVersions.indexOf(x.php_version) > -1 }
           })
           return re;
    }

    return app


    function walkTallies(){
        return getWh().WalkAll()
    }
    function szarBackupAll(){
        return getWh().DoSzarBackupAll()
    }

    function getWh(){
        return app.WebhostingLib(app.knex, app)
    }


    function processStorages(){
       var oStorages = config.get("storages")
       if(!Array.isArray(oStorages)) throw new Error("Storages must be configured as an array")
       if(oStorages.length <= 0) throw new Error("There must be at least one storage configured")


       var constraints = {label:{isString: true}}
       app.storageCategories.forEach(cat=>{
          constraints[cat] = {isPath: true} // this will also normalize the path
       })
       var vali = require("MonsterValidators").ValidateJs()
       app.storages = []
       app.storagePathes = {}
       oStorages.forEach(q=>{
          var d
          if(typeof q == "string")
          q = {"web": q}


          vali.async(q, constraints)
            .then(ad=>{
               d = ad

               if(Object.keys(d).length <= 0) throw new Error("Invalid storage definition, nothing configured")

               var ps = [];
               app.storageCategories.forEach(cat=>{
                 if(!d[cat]) return
                 ps.push(
                   fs.statAsync(d[cat])
                     .catch(x=>{
                        console.error("WARNING, path does not exist; adding anyway", cat, d[cat], x)
                     })
                    .then(s=>{
                       if((s)&&(!s.isDirectory())) throw new Error("Path exists, but it is not a directory")

                       if(!d.label) d.label = d.web;
                       if(!d.label) throw new Error("Invalid storage definition, no label");

                       app.storagePathes[d.label] = d;
                       if(app.storages.indexOf(d.label) < 0)
                         app.storages.push(d.label);
                    })
                  );
               })

               return Promise.all(ps);
            })
            .catch(x=>{
               console.error("Invalid path configured: ", q, x)
               process.reallyExit()
            })
       })



    }



}
