(function(){


	var ValidatorsLib = require("MonsterValidators");
	var vali = ValidatorsLib.ValidateJs();

  const dotq = require("MonsterDotq");
  const fs = dotq.fs();
  const hourMultiplier = 60*60*1000;
  var phpSlowlogParserLib;


    var lib = module.exports = function(app, knex) {


    function slowlogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["sl_timestamp", "sl_appserver", "sl_id"], default: "sl_timestamp"},
               "desc": {isBooleanLazy: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "sl_webhosting": {isString:{lazy:true}},
               "sl_appserver": {isString: true},
               "sl_path": {isString: true},
               "sl_dump": {isString: true},
          })
    }
    function slowlogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("sl_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("sl_timestamp<=?")
          qArr.push(d.notafter)
       }

       if(d.sl_webhosting){
          qStr.push("sl_webhosting=?")
          qArr.push(d.sl_webhosting)
       }

       if(d.sl_appserver){
          qStr.push("sl_appserver=?")
          qArr.push(d.sl_appserver)
       }
       if(d.sl_path){
          qStr.push("sl_path=?")
          qArr.push(d.sl_path)
       }
       if(d.sl_dump){
          qStr.push("sl_dump LIKE ?")
          qArr.push('%'+d.sl_dump+'%')
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }



    var re = {}


    re.Insert = function(in_data){

       //console.log("insert was called", in_data)

       var d;

       return vali.async(in_data, {
       	  sl_webhosting: {presence: true, isString: {lazy:true}},
       	  sl_appserver: {presence: true, isString: true},
          sl_path: {isString: true, default:{value:{}}},
       	  sl_dump: {isString: true, default: {value:""}},
       	  sl_timestamp: {isString: true},
       })
         .then(ad=>{
            d = ad;

            return lookupWebhosting(d)
         })
         .then((wh)=>{
            d.sl_user_id = wh.wh_user_id;

            app.slowlogSync = app.slowlogSync.concat(d);

            var e = extend({}, d);
            e.sl_dump = e.sl_dump.split("\n");

            app.EscalateEvent("WEBHOSTING_SLOWLOG_EVENT", {slowQuery: e});
         })
    }


    re.Query = function (pd) {
           var d
           return slowlogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = slowlogQueryWhere(d,knex.select()
                   .table('slowlog')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined)
                  )
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                return {"events": rows }

              })
    }

    re.Count = function (d) {
           return slowlogQueryParams(d)
              .then(d=>{
                 var s = slowlogQueryWhere(d, knex('slowlog').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }


    re.setupSlowlogWatchers = function(){
       app.slowlogParsers.forEach(x=>{
        console.log("unwatching parser", x)
          x.parser.unwatch();
       });
       app.slowlogParsers = [];

       var nodes = app.config.get("php_distributions");
       if(!nodes) return;


       nodes.forEach(phpNode=>{

           var filename = lib.getSlowlogFilename(phpNode);

           var parser = phpSlowlogParserLib({filename:filename, onEvent: function(event){
               // console.log("parser event !!!", event)

               event.sl_appserver = "php/"+phpNode.php_version;

               return re.Insert(event);
           }})

           app.slowlogParsers.push({filename:filename, parser:parser, phpNode: phpNode});
       })

    }


    if(app.config.get("slowlog_parser")) {
        phpSlowlogParserLib = require("lib-php-slowlog-parser.js");

        if(!app.slowlogSync) app.slowlogSync = [];

        if(!app.slowlogTimer) {
            rescheduleSlowlogSyncTimer();          
        }

        if(!app.slowlogParsers) {
           app.slowlogParsers = [];
           re.setupSlowlogWatchers();
        }

        if(!app.slowlogRotate) {
          setupSlowlogRotator();
        }
    }


    return re;

    function setupSlowlogRotator(){
       setInterval(function(){
           if(!app.slowlogParsers.length) return;

           app.slowlogParsers.forEach(stuff=>{
              console.log("doing slowlog rotation", stuff.filename);
              return fs.truncateAsync(stuff.filename)
                .then(()=>{
                    const phpfpmLib = require("lib-phpfpm.js")(app);
                    return phpfpmLib.ReopenLogPhpFpm();
                })
                .then(()=>{
                    return stuff.parser.setupTail();
                })
                .catch(ex=>{
                   if(ex.code == "ENOENT") return;
                   console.error("Unable to rotate PHP slowlog", stuff, ex);
                })

           })

       }, app.config.get("rotate_slowlog_after_hours")*hourMultiplier);
    }


    function rescheduleSlowlogSyncTimer(){
        app.eventSyncTimer = setTimeout(function(){

            var dataToInsert = app.slowlogSync;
            app.slowlogSync = [];

            return Promise.resolve()
              .then(()=>{
                 if(!dataToInsert.length) return;

                  return knex.transaction(trx=>{

                     return dotq.linearMap({array:dataToInsert,action:function(row){
                        return trx.into("slowlog").insert(row).return()
                     }})

                  })
                  .then(()=>{
                     console.log("Successfully inserted", dataToInsert.length, "slowlog events");
                  })


              })
              .catch(ex=>{
                 // but the show must go on...
                 console.error("We were unable to flush events into slowlog", ex)
              })
              .then(()=>{
                 return rescheduleSlowlogSyncTimer()
              })


        }, app.config.get("slowlog_sync_flush_seconds")*1000)

    }

    function lookupWebhosting(d) {
       const whLib = require("models/webhosting.js")(knex, app);
       return whLib.GetWebhostingById(d.sl_webhosting, true);
    }

}

  lib.getSlowlogFilename= function(phpNode) {
     const path = require("path");
     var filename = path.join(phpNode.basedir, phpNode.slowlog_path||"logs/php-fpm.slow");
     return filename;
  }

})()


