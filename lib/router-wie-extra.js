module.exports = function(app, wieRouter) {

      function szarRestore(req){
            var data = req.body.json;
            data.dir = getWieConfigDir();
            if(req.params.webhosting_id)
               data.dir = path.join(data.dir, req.params.webhosting_id+".json");
            return app.SzarLib.SzarDoRestore(data, req);
      }

    wieRouter.post("/szar/restore/:webhosting_id", function(req,res,next){
        return req.sendTask(szarRestore(req));
      })
    wieRouter.post("/szar/restore/", function(req,res,next){
         return req.sendTask(
            removeExistingWieFiles()
            .then(()=>{
               return szarRestore(req);
           })
         );
      })


   function removeExistingWieFiles(){

       const deleteOld = require("DeleteOld");
       return deleteOld(
          getWieConfigDir(),
          {pattern: /\.json$/}
       );

   }

   function getWieConfigDir(){
   	  var dir = app.config.get("global_wie_config_dir");
   	  if(!dir) throw new MError("NOT_CONFIGURED");
   	  return dir;
   }

}

