var lib = module.exports = function(app) {

    var re = {}
	re.QueryTemplate = function(template_name, dontThrow) {

	  return app.MonsterInfoWebhostingtemplates.GetInfo(template_name)
	    .catch(ex=>{
	    	if(dontThrow) {
	    	  console.error("Error while looking up the template", template_name, ex)
  	    	  return Promise.resolve({})
	        }
	        throw ex
	    })
	}

    return re

}

