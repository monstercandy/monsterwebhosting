(function(){

	module.exports = function(app) {


    const MError = require("MonsterError")
		var lib = {};

	    lib.SzarCheckpoints = function() {

             var params = buildSzarCommand(["rcheckpoints"], null, null, {omitAggregatedOutput:false,executeImmediately: true})

             return app.commander.spawn(params)
               .then(h=>{
                  return h.executionPromise
               })
               .then((o)=>{
                  var re = []
                  //2017/04/01 12:03:59 [INFO] CP: 20170328010604

                  var myString = o.output
                  var myRegexp = /\[INFO\] CP: ([0-9]+)/g;
                  var match = myRegexp.exec(myString);
                  while (match != null) {

                    re.push(match[1])
                    match = myRegexp.exec(myString);

                  }

                  return Promise.resolve(re)
               })
               .catch(ex=>{
                  console.error("error during szar rcheckpoints call (might be safe if there was no backup scheduled yet)", ex)
                  return Promise.resolve([])
               })
        }


        lib.SzarDoRestore = function(in_data, req) {

          return vali.async(in_data, {
              dir: {presence: true, isPath: true},
              checkpoint: {format: {pattern:/^[0-9]+$/}},
          })
          .then(d=>{

             var rest = ["restore", d.dir, d.dir];
             if(d.checkpoint)
               rest.push(d.checkpoint);

             var params = buildSzarCommand(rest);

             app.InsertEvent(req, {e_event_type: "szar-restore", e_other:true});

             return app.commander.spawn(params)
          })

        }


        lib.SzarDoBackup = function(gemitter, req) {

             var params = buildSzarCommand(["backup"], null, gemitter)

             return app.commander.spawn(params)
               .then(h=>{
                   app.InsertEvent(req, {e_event_type: "szar-backup", e_other:true});

                   h.executionPromise.catch(ex=>{
                      console.error("error during szar backup", ex)
                   })
                   .then(()=>{
                       if(gemitter) gemitter.send_stdout_ln("Sending cleanup command")

                       var params = buildSzarCommand(["cleanup"], null, gemitter)

                       return app.commander.spawn(params)
                   })

                   return Promise.resolve(h)
               })
        }

	            function buildSzarCommand(rest, stdoutReader, gemitter) {
	                 if(!rest) rest = []

	                 var cmd = app.config.get("szar_cmd_path");
	                 var cfg = app.config.get("szar_config_path_mc");
	                 if((!cfg)||(!cmd)) throw new MError("SZAR_NOT_CONFIGURED");

	                 var args = [cfg].concat(rest)


	                 var params = {
	                    chain:{
	                      executable: cmd,
	                      args:args,
	                   },
	                   dontLog_stdout: true,
	                   omitAggregatedOutput: true,
	                 }

	                 if(gemitter)
	                    params.emitter = gemitter

	                 if(stdoutReader)
	                    params.stdoutReader = stdoutReader

	                 params.stderrReader = function(line){
	                    console.error("szar error:", line.toString());
	                 }

	                 return params

	            }



      install_cron();

	     return lib;


      function install_cron(){
        if(app.cron_szar) return;
        app.cron_szar = true;

        var csp = app.config.get("cron_mc_szar_backup");
        if(!csp) return;

        const cron = require('Croner');

        cron.schedule(csp, function(){
           console.log("scheduled mc szar backup");
           return lib.SzarDoBackup()
            .catch(ex=>{
                console.error("Unable to create the scheduled backups", ex)
            })

        });
      }


	}


})()
