module.exports = function(basedir, subDirHashes, ownerGid, fs) {

	if(!Array.isArray(subDirHashes))subDirHashes = [subDirHashes]

    const dotq = require("MonsterDotq");
    fs = fs || dotq.fs(); // for mocking
    const path = require("path").posix

    var ops = 0
    var first = -1
	var ps = []
	return dotq.linearMap({array:subDirHashes, action: subDirHash=>{

        var subdirHashKeys =  Object.keys(subDirHash || {});
        return dotq.linearMap({array: subdirHashKeys, action: subdirOwner=>{

			var subdirs = subDirHash[subdirOwner]
			if(!Array.isArray(subdirs)) subdirs = [subdirs]

		    return dotq.linearMap({array: subdirs, action: subdir=>{

				var fulldir = path.join(basedir, subdir)
				if(ops == 0) first = subdirOwner
				var ownerUid = subdirOwner == "first" ? first : subdirOwner
	    	    ops++
	    		return mkdirAndChown(fulldir, ownerUid, ownerGid)


		    }})

        }});


	}})

    function mkdirAndChown(fulldir, owner, group){
		return fs.mkdirAsync(fulldir)
		  .catch(ex=>{
	         if(ex.code == 'EEXIST') return;
			 console.error("cant create ", fulldir, ex)
 			 throw ex
		  })
		  .then(()=>{
		  	 return fs.chownAsync(fulldir, parseInt(owner,10), parseInt(group || -1,10))
		  })

    }

}
