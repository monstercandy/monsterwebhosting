(function(){


	var ValidatorsLib = require("MonsterValidators");
	var vali = ValidatorsLib.ValidateJs();

  const dotq = require("MonsterDotq");
  const fs = dotq.fs();
  const phpaccesslogParserLib = require("lib-php-accesslog-parser.js");


    var lib = module.exports = function(app, knex) {


    function accesslogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["al_timestamp", "al_id", "al_stat_user", "al_stat_system", "al_stat_hits", "al_stat_mempeak", "al_stat_servtime"], default: "al_timestamp"},
               "desc": {isBooleanLazy: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "al_webhosting": {isString:{lazy:true}},
               "al_vhost": {isString: true},
          })
    }
    function accesslogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("al_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("al_timestamp<=?")
          qArr.push(d.notafter)
       }

       if(d.al_webhosting){
          qStr.push("al_webhosting=?")
          qArr.push(d.al_webhosting)
       }
       if(d.al_vhost){
          qStr.push("al_path=?")
          qArr.push(d.al_path)
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }



    var re = {}




    re.Query = function (pd) {
           var d
           return accesslogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = accesslogQueryWhere(d,knex.select()
                   .table('accesslog')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined)
                  )
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                return {"events": rows }

              })
    }

    re.Count = function (d) {
           return accesslogQueryParams(d)
              .then(d=>{
                 var s = accesslogQueryWhere(d, knex('accesslog').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }


    re.process = function(req){
         console.log("processing php accesslog entries");
         var nodes = app.config.get("php_distributions");
         if(!nodes) return;

         const reader = require("FirstLineAsJSON");
         const fpmLib = require("lib-phpfpm.js")(app);

         var sumStat = {};

         return dotq.linearMap({array: nodes, catch: true, action: function(phpNode){

            var filename_official = lib.getAccesslogFilename(phpNode);
            var filename_candidate = filename_official+".tmp";
            console.log("Processing PHP accesslog", phpNode.php_version, filename_official);
            return fs.renameAsync(filename_official, filename_candidate)
              .then(()=>{
                  return fpmLib.ReopenLogPhpFpm(phpNode.php_version);
              })
              .then(()=>{
                  return reader.ReadFileLineByLineAsync(filename_candidate, function(line){
                      var r = phpaccesslogParserLib.processLine(line);
                      if(!r) return;
                      if(!sumStat[r.al_webhosting]) sumStat[r.al_webhosting] = {};
                      if(!sumStat[r.al_webhosting][r.al_vhost]) sumStat[r.al_webhosting][r.al_vhost] = {};

                      Array("al_stat_user","al_stat_system","al_stat_mempeak","al_stat_servtime").forEach(c=>{
                         if(!sumStat[r.al_webhosting][r.al_vhost][c]) sumStat[r.al_webhosting][r.al_vhost][c] = 0;

                         sumStat[r.al_webhosting][r.al_vhost][c] += r[c];
                      })

                      if(!sumStat[r.al_webhosting][r.al_vhost].al_stat_hits) sumStat[r.al_webhosting][r.al_vhost].al_stat_hits = 0;
                      sumStat[r.al_webhosting][r.al_vhost].al_stat_hits++;
                  })

              })
              .then(()=>{
                  return fs.unlinkAsync(filename_candidate);
              })

         }})
         .then(()=>{
            console.log("everything is processed", sumStat);

            if(Object.keys(sumStat).length <= 0) return;
            return knex.transaction(trx=>{
                return dotq.linearMap({array:Object.keys(sumStat), action: function(al_webhosting){
                    return dotq.linearMap({array:Object.keys(sumStat[al_webhosting]), action: function(al_vhost){
                        var row = sumStat[al_webhosting][al_vhost];
                        row.al_vhost = al_vhost;
                        row.al_webhosting = al_webhosting;

                        Array("al_stat_user","al_stat_system","al_stat_mempeak","al_stat_servtime").forEach(c=>{
                         row[c] = +(row[c].toFixed(2));
                        })

                        return trx("accesslog").insert(row);

                    }});

                }})

            })
            .then(()=>{
               console.log("database populated");
            })
         })
         .then(()=>{
            return sumStat;
         })


    }


    installCron();


    return re;

    function installCron(){

       if(app.config.get("php_fpm_accesslog_disabled")) return;

       if(app.cron_php_accesslog_configured) return;

       app.cron_php_accesslog_configured = true;

       const cron = require('Croner');

       var csp = app.config.get("cron_php_accesslog_process")
       if(!csp) return;

       cron.schedule(csp, function(){
            return re.process()
             .catch(err=>{
                console.error("error while processing accesslog entries", err)
             })
       });


    }


    function rescheduleaccesslogSyncTimer(){
        app.eventSyncTimer = setTimeout(function(){

            var dataToInsert = app.accesslogSync;
            app.accesslogSync = [];

            return Promise.resolve()
              .then(()=>{
                 if(!dataToInsert.length) return;

                  return knex.transaction(trx=>{

                     return dotq.linearMap({array:dataToInsert,action:function(row){
                        return trx.into("accesslog").insert(row).return()
                     }})

                  })
                  .then(()=>{
                     console.log("Successfully inserted", dataToInsert.length, "accesslog events");
                  })


              })
              .catch(ex=>{
                 // but the show must go on...
                 console.error("We were unable to flush events into accesslog", ex)
              })
              .then(()=>{
                 return rescheduleaccesslogSyncTimer()
              })


        }, app.config.get("accesslog_sync_flush_seconds")*1000)

    }

}

  lib.getAccesslogFilename= function(phpNode) {
     const path = require("path");
     var filename = path.join(phpNode.basedir, phpNode.slowlog_path||"logs/php-fpm.access");
     return filename;
  }

})()


