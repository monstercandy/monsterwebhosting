
var lib = module.exports = function(knex, app) {

    const DIFF_LOG_DIR_NAME = "difflogs";

    var ValidatorsLib = require("MonsterValidators");
    var Common = ValidatorsLib.array();
  	var vali = ValidatorsLib.ValidateJs();

    const path = require("path").posix;
    const dotqLib = require("MonsterDotq");
    const fs = dotqLib.fs();

    const this_server = app.config.get("mc_server_name");

    // 2017/04/02 11:27:31 [DEBUG] NEW F BZ2: /bm.monstermedia.hu/pages/znew.php
    const logProcessingRegexp = / (CHG|NEW) F (?:GZ|BZ2): \/(.+\.(php|htaccess))$/gm;

    const Token = require("Token");

    const moment = require('MonsterMoment');

    const MError = require('MonsterError');

    const legacy_php_fpm = app.config.get("legacy_php_fpm");

    var re = {}

    re.Insert = function(in_server_data, req){

      var d

      var v = getValidators({no_user_check: in_server_data.no_user_check})
      return vali.async(in_server_data, v)
      	.then(ad=>{

           d = ad
           if(d.wh_docker_hints)
              d.wh_docker_hints = JSON.stringify(d.wh_docker_hints);
           else
              d.wh_docker_hints = "{}";

           if(d.wh_secretcode) return Promise.resolve(d.wh_secretcode)

           return Token.GetTokenAsync(16, "hex")
        })
        .then(secret=>{
           d.wh_secretcode= secret

           return validateTemplateAndOverride(d)
        })
        .then(wh_template_override_str=>{
           d.wh_template_override = wh_template_override_str || "{}"
           delete d.template_override

           // we unfortunately forgot setting default values in the scheme for these ones
            Array('wh_tally_web_storage_mb','wh_tally_mail_storage_mb','wh_tally_db_storage_mb').forEach(k=>{
                d[k] = 0
            })

           return knex
             .insert(d)
             .into("webhostings")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })

        })
        .then((id)=>{
           app.InsertEvent(req, {e_event_type: "webhosting-create", e_other: {wh_id:id}, e_user_id: d.wh_user_id})

           return Promise.resolve(id)
        })


	}

  re.GetBandwidthLimitedWebstores = function(){
    var xre = {};
    return re.GetAllWebhostings()
      .then(whs=>{
         return dotqLib.linearMap({array: whs, action: function(wh){
            return wh.QueryTemplate()
              .then(()=>{

                  var keys = Object.keys(wh.template.t_bandwidth_limit||{});
                  if(!keys.length) return;

                  xre[wh.wh_id] = wh.template.t_bandwidth_limit;
              })
         }})
      })
      .then(()=>{
         return xre;
      })
  }


  re.SendTallyWarnings = function(){

    return re.GetAllWebhostings()
      .then((whs)=>{
         var ps = []
         whs.forEach(wh=>{
            ps.push(
                wh.QueryTemplate()
                  .then(()=>{
                      if(!wh.template.t_storage_max_quota_soft_mb) return; // quota lookup failed?...

                      // is it the same as the last time?
                      if(wh.wh_tally_sum_storage_mb_last == wh.wh_tally_sum_storage_mb) return;

                      if(wh.template.t_storage_max_quota_soft_mb * app.config.get("tally_warning_threshold_multiplier") < wh.wh_tally_sum_storage_mb)
                          return wh.SendTallyWarning()
                  })
                  .catch(ex=>{
                     console.error("error during sending tally warnings for", wh.wh_id, ex)
                  })
            )
         })
         return Promise.all(ps)
      })

  }


  re.DoSzarBackupAll = function(gemitter){

     const MonsterMoment = require("MonsterMoment")

     var ts_begin = new MonsterMoment();
     var stats = {
        success: 0,
        fail: 0,
        skip: 0,
        storages: 0,
        szars: 0,
        users: 0,    
        begin: ts_begin.toISOStringWithTimeZone(),
     };

     var users = {};

    return re.GetAllWebhostings()
      .then((whs)=>{

             return dotqLib.linearMap({array: whs, action: function(wh){

                stats.storages++;
                if(!users[wh.wh_user_id]) {
                    users[wh.wh_user_id];
                    stats.users++;
                }

                return runSzarBackupIfStoreExists(wh, "web")
                  .then(()=>{
                      return runSzarBackupIfStoreExists(wh, "mail")
                  })


             }})
             .then(()=>{
                console.log("Walked through all of the webstores, now sending an email summary");

                var ts_end = new MonsterMoment();
                stats.end = ts_end.toISOStringWithTimeZone();
                stats.duration_sec = MonsterMoment.base.duration(ts_end.diff(ts_begin)).asSeconds();
                var mailer = app.GetMailer()

                var x = {}
                x.toAdmin = true;
                x.template = "webhosting/backup-stats"
                x.context = {stats: stats};
                mailer.SendMailAsync(x).catch(console.error);
             })
             .catch(ex=>{
                console.error("Unrecoverable error during szar backup", ex);
                throw ex;
             })


             function runSzarBackupIfStoreExists(wh, category) {
                 stats.szars++;
                 var backupDir = wh.extras[category+"_path"];
                 return fs.accessAsync(backupDir)
                   .then(()=>{
                      // it exists...
                      var msg = "Starting "+category+" backup of "+wh.wh_id;
                      if(gemitter) gemitter.send_stdout_ln(msg);
                      console.log(msg)
                      return wh.SzarDoBackup(category, gemitter, null, true)
                   })
                   .then(x=>{
                      console.log("Backup success!")
                      stats.success++;
                      return x;
                   })
                   .catch(err=>{
                      // we can swallow the ENOENT stuff
                      if(err.code == 'ENOENT') {
                         console.log("skipping szar-backup of", wh.wh_id, category,"because directory does not exist", backupDir);
                         stats.skip++;
                         return;
                      }
                      stats.fail++;
                      if(gemitter) gemitter.send_stdout_ln("Error during szar backup "+category+" of "+wh.wh_id)
                      console.error("something wrong happened during szar", category, "backup", wh.wh_id, err);
                      app.InsertException(null, {message:"SZAR_BACKUP_FAILED", category: category, wh_id: wh.wh_id}, err);

                   })

             }


         })

  }


  re.WalkAll = function(){
    var allStats = {}
    console.log("WalkAll just invoked");
    return re.GetAllWebhostings()
      .then((whs)=>{
         return dotqLib.linearMap({array: whs, catch: true, action: wh=>{
                return wh.Walk()
                  .then(re=>{
                      allStats[wh.wh_id] = re
                  })
         }});

      })
      .then(()=>{
         // and now lets store them inside a transaction
         return re.StoreWalks(allStats)
      })

  }

  re.DoSendTalliesToFtp = function(){
     return re.GetAllWebhostings()
       .then(webhostings=>{
             return sendTalliesToFtp(webhostings)
       })
  }

  re.ProcessTalliesForWebhostings = function(in_data, method) {

     return Promise.resolve()
       .then(()=>{
            var flush = {}
            Array("web", "mail", "db").forEach(x=>{
               var k = "wh_tally_"+x+"_storage_flush"
               if(in_data[k]) {
                  delete in_data[k]
                  flush["wh_tally_"+x+"_storage_mb"] = 0
               }
            })
            if(!Object.keys(flush).length) return Promise.resolve()

            return knex("webhostings").update(flush)
       })
       .then(()=>{

            var ps = []
            Object.keys(in_data).forEach(wh_id=>{
               var pl = in_data[wh_id]
               ps.push(
                 re.GetWebhostingById(wh_id)
                  .then(hosting=>{
                      return hosting[method](pl)
                  })
                  .catch(ex=>{
                     console.error("Unable to save tally details for", wh_id, ex)
                  })
               )

            })

            return Promise.all(ps)

       })

  }

  re.StoreWalks = function(stats){
     var webhostings = []
     console.log("Storing walk results");
     return knex.transaction(trx=>{

        var whLib = lib(trx, app)

        var ps = []
        return dotqLib.linearMap({array:Object.keys(stats), catch: true, action: wh_id=>{
             return whLib.GetWebhostingById(wh_id)
                  .then(wh=>{
                     webhostings.push(wh)
                     return wh.SetTallyDetails(stats[wh_id])
                  })

        }})

     })
     .then(()=>{
        console.log("Sending tallies to the FTP subsystem");
        return sendTalliesToFtp(webhostings)
     })
  }


	re.GetAllWebhostings = function(filters) {

    var whereExpressions = ["1"]
    var whereParams = []

    Object.keys(filters||{}).forEach(n=>{
       if(Array("wh_id", "wh_user_id").indexOf(n) > -1) {
            whereExpressions.push(n+"=?")
            whereParams.push(filters[n])
       }
    })

    var whereStr = whereExpressions.join(" AND ")

		return knex.select().from("webhostings")
       .whereRaw(whereStr, whereParams)
		   .then(rows=>{
		   	   return webhostingRowsToObject(rows)
		   })

	}

  re.CloneSettings= function(in_data){
     var d = {};
     Array(
       "wh_user_id",
       "wh_php_version",
       "wh_is_readonly",
       "wh_ftp_accepted_from_latest_admin_ip_only",
       "wh_ftp_accepted_from_foreign_ip",
       "wh_notify_script_differences",
       "wh_notify_foreign_ftp_login_events",
       "wh_tally_web_storage_mb",
       "wh_tally_mail_storage_mb",
       "wh_tally_db_storage_mb",
       "wh_max_execution_second",
       "wh_php_fpm_conf_extra_lines",
       "wh_tally_sum_storage_mb",
       "wh_template",
       "wh_template_override",
       "wh_installatron_server",
       "wh_docker_hints",
     ).forEach(x=>{
         d[x] = in_data[x];
     });

     Array(
       "wh_tally_web_storage",
       "wh_tally_mail_storage",
       "wh_tally_db_storage",
       "wh_docker_hints",
     ).forEach(x=>{
         d[x] = JSON.stringify(in_data[x]);
     });

     if(!d.wh_docker_hints) d.wh_docker_hints = "{}";

     return knex("webhostings").update(d).whereRaw("wh_id=?", [in_data.wh_id]).then(()=>{
        return re.GetWebhostingById(in_data.wh_id, true)
     })
     .then(webhosting=>{
        // resaving this one
        return webhosting.SavePhpFpmConfig();
     })
  }

  re.GetWebhostingById = function(id, skipTemplate) {

     var wh
     return re.GetAllWebhostings({wh_id: id})
       .then(rows=>{
           wh = Common.EnsureSingleRowReturned(rows, "WEBHOSTING_NOT_FOUND", {wh_id: id})

           if(skipTemplate) return wh;

           return wh.QueryTemplate()
       })
  }


  function queryTemplateForWebhosting(webhosting, dontThrow) {

        return app.templateLib.QueryTemplate(webhosting.wh_template, dontThrow)
          .then(template=>{

              template = simpleCloneObject(template)

              var to = {}
              try {
                to = JSON.parse(webhosting.wh_template_override)
              }
              catch(ex) {
              }

              Object.keys(to).forEach(q=>{
                 template[q] = to[q]
              })

              return Promise.resolve(template)
          })

  }

    function webhostingRowToObject(webhosting) {

        function getPhpFpmLib(){
           const phpfpmlib = require("lib-phpfpm.js")
           return phpfpmlib(app, webhosting)
        }

        function sendMailAsync(template){
             var mailer = app.GetMailer()

             var x = {}
             x.toAccount = {user_id: webhosting.wh_user_id, emailCategory: "TECH"}
             x.template = "webhosting/"+template
             x.context = {webhosting: webhosting, server: this_server}

             return mailer.SendMailAsync(x)
               .catch(ex=>{
                  console.error("We were unable to send notification email", ex);
               })
        }

        Array("wh_tally_web_storage","wh_tally_mail_storage","wh_tally_db_storage","wh_docker_hints").forEach(k=>{
          try {
            webhosting[k] = JSON.parse(webhosting[k])
          }catch(ex){
            console.error("Invalid value saved in "+k, ex)
            webhosting[k] = {}
          }
        })


        // this is not managed here anymore, goto iptables microservice instead
        delete webhosting.wh_firewall

        webhosting.extras = {}
        if(app.storagePathes[webhosting.wh_storage]) {
             if(app.storagePathes[webhosting.wh_storage].web){
               webhosting.extras.web_path = path.join(app.storagePathes[webhosting.wh_storage].web,  webhosting.wh_id + "-" + webhosting.wh_secretcode)
             }
             if(app.storagePathes[webhosting.wh_storage].mail)
               webhosting.extras.mail_path = path.join(app.storagePathes[webhosting.wh_storage].mail,  ""+ webhosting.wh_id)
        }

        webhosting.hooks = {
           "deactivate": function(params, gemitter){
                 sendMailAsync("webhosting-deactivated")
                 return Promise.resolve()
            },
           "reactivate": function(params, gemitter){
                 sendMailAsync("webhosting-reactivated")
                 return Promise.resolve()
            },
           "uninit": function(params, gemitter){
              // console.log("!!! removing python apps")
              // deleting python apps
              if(app.pythonLib) {
                return app.pythonLib.DeletePythonAppsOfWebhosting(webhosting)
              }
           },

           "remove": function(params, gemitter){
                // we remove the phpfpm config first, since it wont start if the dir does not exist
                return webhosting.RemovePhpFpmConfig()
                .then((deleted)=>{

                   // console.log("!!! rehashing php-fpm")

                   if((deleted)&&(app.config.get("reload_phpfpm_after_webhosting_delete")))
                     return getPhpFpmLib().RehashPhpFpm(null, true)
                   return Promise.resolve()
                })
                .then(()=>{
                    if(params.no_rmdir)
                      return;

                    return fs.ensureRmdiredAsync(webhosting.extras.web_path);
                })
                .then(()=>{
                  if(params.no_rmdir)
                     return;

                  if(!webhosting.extras.mail_path)
                     return;

                  return fs.ensureRmdiredAsync(webhosting.extras.mail_path);
                })
                .then(()=>{
                  // console.log("!!! and now deleting the webhosting")

                  // the rest are the database related stuffs
                  return webhostingDelete();
                })
                .then(x=>{
                       sendMailAsync("webhosting-removed")

                       return Promise.resolve(x)
                })
           },

           "create": function(params, gemitter){
                return Promise.resolve()
                  .then(()=>{
                      if(params.no_mkdir) return Promise.resolve()

                      const mksubdir = require("mksubdir.js")

                      var maindir = {}
                      maindir[webhosting.wh_id] = "/" // subdirhash

                      return mksubdir(
                         webhosting.extras.web_path, // basedir
                         [
                            maindir,
                            app.config.get("mksubdir") // subdirhash
                         ],
                         app.config.get("owner_gid") // ownerGid
                      )
                      .then(()=>{
                          return mksubdir(
                             webhosting.extras.mail_path, // basedir
                             [
                                maindir,
                             ],
                             app.config.get("owner_gid") // ownerGid
                          )
                      })

                  })
                  .then(()=>{
                      var phplib = getPhpFpmLib()

                      return phplib.SavePhpFpmConfig()
                       .then((created)=>{
                         // console.log("!!! saving php fpm config inside init hook")
                         if((created)&&(app.config.get("reload_phpfpm_after_webhosting_create")))
                           return phplib.RehashPhpFpm(null, true)
                         return Promise.resolve()
                      })

                  })
                  .then(()=>{
                       // we leave this as unhandled
                       sendMailAsync("webhosting-created")

                       return Promise.resolve()
                  })

           },

           "init": function(params, gemitter){
               return Promise.resolve();
           }
        }

            webhosting.Cleanup = function() {
                delete webhosting.wh_mail_service_account_password;
                delete webhosting.hooks;
                return webhosting;
            }

            function webhostingDelete(){

                return knex("webhostings")
                       .whereRaw("wh_id=?", [webhosting.wh_id])
                       .del()
                  .then(()=>{
                    app.InsertEvent(null, {e_event_type: "webhosting-remove", e_other: {wh_id: webhosting.wh_id}, e_user_id: webhosting.wh_user_id})

                    return Promise.resolve()
                 })

            }

            webhosting.RemovePhpFpmConfig = function(){
                if(!legacy_php_fpm) return Promise.resolve(false); // it was not deleted.
                return getPhpFpmLib().RemovePhpFpmConfig()
            }

            webhosting.SavePhpFpmConfig = function(){
                if(!legacy_php_fpm) return Promise.resolve(false); // it was not created
                return getPhpFpmLib().SavePhpFpmConfig()
            }
            webhosting.ReadPhpFpmConfig = function(){
                return getPhpFpmLib().ReadPhpFpmConfig()
            }

            webhosting.QueryTemplate = function(){

               return queryTemplateForWebhosting(webhosting, true)
                 .then(template => {
                    webhosting.template = template
                    return Promise.resolve(webhosting)
                  })
            }

            webhosting.Change = function(in_data){
                var v = getValidators()
                delete v.wh_id
                delete v.wh_user_id
                delete v.wh_storage
                delete v.wh_secretcode

                Object.keys(v).forEach(q=>{
                  delete v[q].presence
                  delete v[q].default
                })

                var d
                return vali.async(in_data, v)
                  .then(ad=>{
                     d = ad
                     d["updated_at"] = moment.now()

                     if(d.wh_docker_hints)
                        d.wh_docker_hints = JSON.stringify(d.wh_docker_hints);

                     return validateTemplateAndOverride({wh_template: d.wh_template||webhosting.wh_template, template_override: d.template_override})
                  })
                 .then(wh_template_override_str=>{

                     if(wh_template_override_str)
                       d.wh_template_override = wh_template_override_str
                     delete d.template_override

                     return Promise.resolve()
                   })
                  .then(()=>{
                       if(typeof d.wh_is_expired == "undefined") return Promise.resolve()

                       webhosting.wh_is_expired = d.wh_is_expired
                       var hookcommon = require("lib-common.js")(app)
                       return hookcommon.runHooks([webhosting], d.wh_is_expired ? "deactivate" : "reactivate")

                  })
                  .then(()=>{

                     /**
                     TODO: anything additional that could be needed for wh_mail_service_account_password
                     */

                     return knex("webhostings")
                       .update(d)
                       .whereRaw("wh_id=?",[webhosting.wh_id])

                  })

            }

            webhosting.SendTallyWarning = function(){
                 return (webhosting.template ? Promise.resolve() : webhosting.QueryTemplate())
                   .then(()=>{

                       var mailer = app.GetMailer()

                       var x = {}
                       x.toAccount = {user_id: webhosting.wh_user_id, emailCategory: "TECH"}
                       x.context = {
                         wh: {}, 
                         server: this_server,
                         stats: {maxSizeMb: webhosting.template.t_storage_max_quota_soft_mb, actSizeMb: webhosting.wh_tally_sum_storage_mb}
                       }
                       Array("wh_name", "wh_tally_sum_storage_mb", "wh_tally_web_storage", "wh_tally_mail_storage", "wh_tally_db_storage").forEach(function(pf){
                           x.context.wh[pf] = webhosting[pf]
                           var mb = pf+"_mb"
                           if(webhosting[mb])
                              x.context.wh[mb] = round2(webhosting[mb])
                       })
                       x.template = x.context.stats.maxSizeMb < x.context.stats.actSizeMb ? "webhosting/storage-tally-error" : "webhosting/storage-tally-warning"

                       return mailer.SendMailAsync(x)
                         .then(()=>{
                            return knex("webhostings").update({wh_tally_sum_storage_mb_last: webhosting.wh_tally_sum_storage_mb}).whereRaw("wh_id=?",[webhosting.wh_id]).return()
                         })

                   })
            }

            webhosting.SetTallyDetails = function(in_data, passToFtp){
                return vali.async(in_data, {
                    "wh_tally_web_storage": {isSimpleObject: true},
                    "wh_tally_mail_storage": {isSimpleObject: true},
                    "wh_tally_db_storage": {isSimpleObject: true},

                    "wh_tally_web_storage_mb": {isDecimal: true},
                    "wh_tally_mail_storage_mb": {isDecimal: true},
                    "wh_tally_db_storage_mb": {isDecimal: true},
                })
                .then(d=>{
                   var mb = {}
                   var extras = {}
                   Object.keys(d).forEach(kmain=>{
                      var sum = 0
                      Object.keys(d[kmain]).forEach(k=>{
                         sum += parseFloat(d[kmain][k])
                      })

                      if(kmain.match(/_mb$/)) return

                      extras[kmain] = d[kmain]
                      mb[kmain+"_mb"] = d[kmain+"_mb"] || sum
                   })

                   return webhosting.SetTallies(mb, extras, passToFtp)
                })
            }

            webhosting.SetTallies = function(in_data, extras, passToFtp){

                var d
                return vali.async(in_data, {
                    "wh_tally_web_storage_mb": {isDecimal: true},
                    "wh_tally_mail_storage_mb": {isDecimal: true},
                    "wh_tally_db_storage_mb": {isDecimal: true},
                })
                .then(ad=>{
                    d = ad

                    if(Object.keys(d).length <= 0) throw new MError("NOTHING_TO_CHANGE")

                      if(extras) {
                        Object.keys(extras).forEach(k=>{
                          d[k] = JSON.stringify(extras[k])
                        })
                      }

                      Object.keys(d).forEach(k=>{
                        if(k.match(/_mb$/))
                           d[k] = round2(d[k])
                         webhosting[k] = d[k]
                      })

                      webhosting.wh_tally_sum_storage_mb= round2(0 + webhosting.wh_tally_web_storage_mb + webhosting.wh_tally_mail_storage_mb + webhosting.wh_tally_db_storage_mb)
                      d.wh_tally_sum_storage_mb = webhosting.wh_tally_sum_storage_mb

                     return knex("webhostings")
                       .update(d)
                       .whereRaw("wh_id=?",[webhosting.wh_id])
                })
                .then(()=>{
                     if((!passToFtp)||(!d.wh_tally_web_storage_mb)) return Promise.resolve()


                     // pass through the ftp
                     return sendTalliesToFtp([webhosting])
                })

            }

            /*
            * NOTE: This is currently implemented as an in-node process without any privilege dropping.
            * This can be exploited via TOCTOU attacks, but the only gain an attacker would have is
            * overdrawing its tally (resulting in blocking himself out of the system)
            *
            * This method can be called by sysops only anyway, so the travversed directory pathes that
            * might be displayed while walking the storage, don't represent any threat.
            *
            * For this reason we consider this method to be acceptable.
            */
            webhosting.Walk = function(gemitter) {

               const dirs_to_skip = app.config.get("walk_skip_directories")
               var wh_tally_web_storage_b = 0
               var wh_tally_web_storage = {}

               var mainRoot = webhosting.extras.web_path
               console.log("walking over webhosting with mainRoot", mainRoot);
               var walker = app.GetWalker();
               return walker({
                  basedir: mainRoot,
                  emitter: gemitter,
               })
               .then(wh_tally_web_storage=>{
                      var re = {wh_tally_web_storage_mb: 0, wh_tally_web_storage: wh_tally_web_storage }
                      Object.keys(wh_tally_web_storage).forEach(k=>{
                         re.wh_tally_web_storage_mb += wh_tally_web_storage[k];
                      })

                      console.log("all done walking:", webhosting.wh_id, re);
                      if(gemitter) gemitter.send_stdout_ln("RESULT: "+ JSON.stringify(re))

                      return re;
               })

            }


            webhosting.SzarSubrepoName = function(cat){
                 var server_name = this_server;
                 if(!server_name)throw new MError("SZAR_NOT_CONFIGURED");
                 return `${server_name}-${webhosting.wh_id}-${cat}`;
            }

            function buildSzarCommand(dest, rest, stdoutReader, gemitter, extra) {
                 if(!rest) rest = []

                 var szar_subrepo = webhosting.SzarSubrepoName(dest)
                 var cmd = app.config.get("szar_cmd_path")
                 var cfg = app.config.get("szar_config_path_"+dest)
                 if((!cfg)||(!cmd)) throw new MError("NOT_CONFIGURED")

                 var args = [cfg].concat(rest)


                 var backupdir = webhosting.extras[dest+"_path"]
                 if(!backupdir) throw new MError("Storage not configured")

                 var params = {
                    chain:{
                      executable: cmd,
                      args:args,
                      spawnOptions:{env:{
                        "SZAR_chroot": backupdir,
                        "SZAR_backupdir": "/",
                        "SZAR_subrepo": szar_subrepo,
                        "SZAR_user": webhosting.wh_id,
                        "SZAR_group": app.config.get("szar_cmd_group"),
                      }
                     }
                   },
                   dontLog_stdout: true,
                   omitAggregatedOutput: true,
                 }

                 if(gemitter)
                    params.emitter = gemitter

                 if(stdoutReader)
                    params.stdoutReader = stdoutReader

                 params.stderrReader = function(line){
                    console.error("szar error:", line.toString());
                 }

                 if(extra)
                   extend(params, extra);

                 return params

            }

            webhosting.GetDiffLogPathes=function(){
              var now = moment.nowRaw()
              var re = {}
              re.fullDir = path.join(webhosting.extras.web_path, DIFF_LOG_DIR_NAME)
              re.filename = "diffs-"+now+".log"
              re.diffFilename = "/" + DIFF_LOG_DIR_NAME + "/" + re.filename
              re.fullFile = path.join(re.fullDir, re.filename)

              return re
            }

            function ensureDirExists(dir){
              return fs.mkdirAsync(dir)
                .catch(err=>{
                  if(err.code != "EEXIST") throw err
                  return Promise.resolve()
                })
                .then(()=>{
                  return Promise.resolve(dir)
                })
            }

            function getDiffLogStream(diffPathes){
                return ensureDirExists(diffPathes.fullDir)
                  .then(()=>{

                     var logger = fs.createWriteStream(diffPathes.fullFile, {flags: 'a'})
                     return Promise.resolve(logger)
                  })
                  .catch(ex=>{
                     console.error("Unable to write difflog:", webhosting.wh_id, ex)
                     return Promise.resolve({
                        write: function(){},
                        end: function(){},
                     })
                  })

            }

            webhosting.SzarCheckpoints = function(dest) {

                 var params = buildSzarCommand(dest, ["rcheckpoints"], null, null, {omitAggregatedOutput:false,executeImmediately: true})


                 return app.commander.spawn(params)
                   .then(h=>{
                      return h.executionPromise
                   })
                   .then((o)=>{
                      var re = []
                      //2017/04/01 12:03:59 [INFO] CP: 20170328010604

                      var myString = o.output
                      var myRegexp = /\[INFO\] CP: ([0-9]+)/g;
                      var match = myRegexp.exec(myString);
                      while (match != null) {

                        re.push(match[1])
                        match = myRegexp.exec(myString);

                      }

                      return Promise.resolve(re)
                   })
                   .catch(ex=>{
                      console.error("error during szar rcheckpoints call (might be safe if there was no backup scheduled yet)", ex)
                      return Promise.resolve([])
                   })
            }


            webhosting.SzarDoRestore = function(category, in_data, req) {

              return vali.async(in_data, {
                  dir: {presence: true, isPath: true},
                  checkpoint: {format: {pattern:/^[0-9]+$/}},
              })
              .then(d=>{

                 var rest = ["restore", d.dir, d.dir];
                 if(d.checkpoint)
                   rest.push(d.checkpoint);

                 var params = buildSzarCommand(category, rest, null, null, {
                   filter_stdout: function(line){
                      return line.toString().replace(webhosting.extras[category+"_path"], "[storage root]")
                   }
                 })

                 app.InsertEvent(req, {e_event_type: "szar-restore", e_other:true});

                 return app.commander.spawn(params)
              })

            }

            webhosting.SzarDoBackup = function(dest, gemitter, req, completeResult) {
                 var allChanges = 0
                 var max_changed_scripts_in_email = app.config.get("max_changed_scripts_in_email")
                 var logReader = null
                 var changedScripts = []
                 var getLogger
                 var logger
                 var diffPathes = webhosting.GetDiffLogPathes()
                 var tobeLogged = []
                 if((webhosting.wh_notify_script_differences)&&(dest == "web")) {
                    logReader = function(line){
                       // some logic that would process backup output

                      var match = logProcessingRegexp.exec(line);
                      while(match != null) {
                        // console.log("match!", match)
                        var script = match[2]

                        if(changedScripts.length < max_changed_scripts_in_email)
                           changedScripts.push(script)

                        allChanges++

                        if(!getLogger)
                            getLogger = getDiffLogStream(diffPathes)
                             .then(alogger=>{
                                 logger = alogger
                                 tobeLogged.forEach(script=>{
                                    logger.write(`${script}\n`)
                                 })
                                 tobeLogged = []
                             })

                        if(logger)
                          logger.write(`${script}\n`)
                        else
                          tobeLogged.push(script)

                        match = logProcessingRegexp.exec(line);
                      }


                    }
                 }

                 var params = buildSzarCommand(dest, ["backup"], logReader, gemitter)

                 var remove_difflogs_older_than_days = app.config.get("remove_difflogs_older_than_days")
                 return app.commander.spawn(params)
                   .then(h=>{
                       app.InsertEvent(req, {e_event_type: "szar-backup", e_other:true});

                       var mainException;
                       var completeResultProm = h.executionPromise.catch(ex=>{
                          mainException = ex;
                          console.error("error during szar backup", dest, webhosting.wh_id, ex)
                       })
                       .then(()=>{
                           if(gemitter) gemitter.send_stdout_ln("Sending cleanup command")

                           var params = buildSzarCommand(dest, ["cleanup"], null, gemitter)

                           return app.commander.spawn(params)
                       })
                       .then((cleanup)=>{
                          // cleaning up old difflog files
                         cleanup.executionPromise.catch(ex=>{
                            console.error("error during szar backup cleanup", dest, webhosting.wh_id, ex);
                         })

                         const deleteOld = require("DeleteOld")
                         return fs.statAsync(diffPathes.fullDir)
                            .then(()=>{
                                return deleteOld(
                                  diffPathes.fullDir,
                                  {olderThanDays: remove_difflogs_older_than_days, pattern: /\.log$/}
                                )
                            })
                            .catch(ex=>{
                                if(ex.code != "ENOENT")
                                   console.error("Error during removing old difflog backup files", ex)
                            })

                       })
                       .then(()=>{
                          if(!logReader) return
                          if(!changedScripts.length) return
                          if(logger)
                              logger.end()

                          if(!app.config.get("send_email_notification_about_changed_scripts")) return

                          var mailer = app.GetMailer()

                          var x = {}
                          x.toAccount = {user_id: webhosting.wh_user_id, emailCategory: "TECH"}
                          x.context = {
                            wh_name: webhosting.wh_name, 
                            server: this_server,
                            allChanges: allChanges, 
                            remove_difflogs_older_than_days: remove_difflogs_older_than_days, 
                            changedScripts: changedScripts, 
                            diffFilename:diffPathes.diffFilename
                          };
                          x.template = "webhosting/diff-log"

                          return mailer.SendMailAsync(x)
                       })
                       .then(()=>{
                          if((completeResultProm)&&(mainException))
                            throw mainException;

                       })

                       if(completeResult)
                         return completeResultProm;

                       return h;
                   })
            }
            webhosting.SzarJump = function(dest) {
                 var szar_subrepo = webhosting.SzarSubrepoName(dest)

                 var hmac_secret = app.config.get("szar_web_auth_hmac")
                 var url_base = app.config.get("szar_web_public_url_base")

                 if((!hmac_secret)||(!url_base))throw new MError("NOT_CONFIGURED")
                 var now = moment.nowUnixtime()
                 var pl = `${szar_subrepo}|${now}`

                 const crypto = require('crypto');
                 const hmac = crypto.createHmac('sha1', hmac_secret);

                 hmac.update(pl);
                 var token = now+"-"+ hmac.digest('hex')

                 return {url:url_base + "/?r="+szar_subrepo+"&a="+token}
            }


    }

    function validateTemplateAndOverride(d) {

        if(!d.template_override) return Promise.resolve()

        var template
        return app.templateLib.QueryTemplate(d.wh_template)
          .then(aTemplate=>{
              template = aTemplate

              return vali.async(d.template_override, ValidatorsLib.WebhostingTemplateValidators)
          })
          .then(to=>{
             var re = {}

             Object.keys(to).forEach(q=>{
                if(to[q] != template[q])
                  re[q] = to[q]
             })

             return Promise.resolve(JSON.stringify(re))
          })

    }

    function webhostingRowsToObject(rows) {

    	  rows.forEach(template=>{
           webhostingRowToObject(template)
    	  })

        Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }


    return re



    function getValidators(params) {
      var re = {
        "wh_id": {isInteger: {lazy:true}},
        "wh_name": {presence: true, isString: true},
        "wh_user_id": {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
        "wh_storage": {presence: true, inclusion: app.storages },
        "wh_secretcode": {isString: true},
        "wh_template": {presence: true, isString: true},

        "wh_php_version": {inclusion: app.getPhpVersions(true), default: {value:""}},

        "wh_is_expired": {isBooleanLazy: true, default: {value:false}},
        "wh_is_readonly": {isBooleanLazy: true, default: {value:false}},

        "wh_installatron_server": {isString: {emptyOk: true, strictName:true}},

        "wh_ftp_accepted_from_latest_admin_ip_only": {isBooleanLazy: true, default: {value:false}},
        "wh_ftp_accepted_from_foreign_ip": {isBooleanLazy: true, default: true},
        "wh_notify_script_differences": {isBooleanLazy: true, default: {value:false}},
        "wh_notify_foreign_ftp_login_events": {isBooleanLazy: true, default: true},

        "wh_max_execution_second": {isDecimal: true, default: {value:0}},
        "wh_php_fpm_conf_extra_lines": {isString: true},

        "wh_mail_service_account_password": {isString: true},

        "wh_docker_hints": {isObject: true},

        "template_override": {isObject: true},

      }

      if((params)&&(params.no_user_check))
        delete re.wh_user_id;

      return re;
    }

    function round2(num){
       return Math.round(num * 100) / 100
    }

    function sendTalliesToFtp(webhostings) {
        var d = []
        webhostings.forEach(webhosting=>{
          d.push({wh_id: webhosting.wh_id, wh_tally_web_storage_mb: webhosting.wh_tally_web_storage_mb, wh_tally_sum_storage_mb: webhosting.wh_tally_sum_storage_mb})
        })
        return app.GetFtpMapiPool().postAsync("/s/[this]/ftp/sites/tallies", d)
    }

}
