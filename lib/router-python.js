var lib = module.exports = function(app, router, common) {

    const path = require('path')
    const mq = require("MonsterDotq")
    var fs = mq.fs();

    const MError = require("MonsterError")

   var PathSplitRegexp = new RegExp('(\\d+)-(.+?).ini$')

   const firstline = mq.single(require("FirstLineAsJSON"))

    var vali = require("MonsterValidators").ValidateJs()



    router.post("/:webhosting_id/python/:python_app_name/restart", restartHelper())
    router.post("/:webhosting_id/python/:python_app_name/regenerate", restartHelper())

    function restartHelper(){
       return getWebHostingByIdPythonAppExpress(function (req, res, next) {
          return req.sendOk(
             writePythonAppConfig(req.hosting, req.python_app)
             .then(()=>{
                 app.InsertEvent(req, {e_event_type: "python-config", e_other:true});
             })
          )
      })
    }

    router.post("/:webhosting_id/python/:python_app_name/requirements",  getWebHostingByIdPythonAppExpress(function (req, res, next) {
          return req.sendPromResultAsIs(
             vali.async(req.body.json,{
                 cleanup: {isBooleanLazy: true, default: {value: false}},
                 upgrade: {isBooleanLazy: true}
              })
              .then(j=>{

                 var req_cmd = app.config.get("python_requirement_command")
                 if(!req_cmd) throw new Error("requirement command not configured")

                 var args = [req.hosting.wh_id, req.python_app.python, j.cleanup ? "1": "0"]
                 if(j.upgrade)
                    args.push("1")



                 var command = {executable: req_cmd, args: args}

                 var max_req_lines = app.config.get("python_max_req_lines")
                 command.spawnOptions = { env: {MAX_REQ_LINES: max_req_lines} }

                 return app.commander.spawn(command)

              })
              .then(h=>{
                  app.InsertEvent(req, {e_event_type: "python-requirements", e_other:true});

                  return Promise.resolve({id:h.id})
              })

          )
    }))



    router.route("/:webhosting_id/python/:python_app_name")
      .get(getWebHostingByIdPythonAppExpress(function (req, res, next) {
          req.sendResponse(req.python_app)

      }))
      .delete(getWebHostingByIdPythonExpress(function(req,res,next){
          return deletePythonAppsAndSend(req.hosting.wh_id, req.params.python_app_name, req)
            .then(()=>{
                  app.InsertEvent(req, {e_event_type: "python-removed", e_other:true});
            })

      }))


    function writePythonAppConfig(webhosting, data) {

        return Promise.resolve()
          .then(()=>{

                var baseport = 8000
                var port = baseport + parseInt(webhosting.wh_id, 10) - 16387
                if((port > 60000)||(port < 8000)) throw new MError("INTERNAL_ERROR",port, "invalid port: "+port)

                data.port = port

                var c = {"python_app_name":data.python_app_name,"python": data.python, "chdir": data.chdir, "module": data.module, "port": port}
                data.cstr = JSON.stringify(c)

                return fs.readFileAsync(app.config.get("python_uwsgi_template_path"), "utf8")
            })
            .then(template=>{
                 var configStr = require("Replacer")(template, [webhosting, data])

                 return fs.writeFileAsync(getConfigFilePath(webhosting, data), configStr)

            })

    }


    router.route("/:webhosting_id/python")
      .put(getWebHostingByIdPythonExpress(function (req, res, next) {

          var data

          var supported = app.getSupportedPythonMajorVersions()
       var constraints = {
           python: {inclusion:supported},
           python_app_name: {presence: true, isString: {strictName: true}},
           chdir: {presence: true,  isPath:true, format: /^\/wsgi-root\/[a-zA-Z0-9\-\/\.]+$/},
           module: {presence: true, isPath:{maxPathLevels:1, lazy: true, stripHeadingSlash: true, stripTailingSlash: true}},
       }
       if(supported.length > 0)
         constraints.python.default = supported.indexOf("python3") > -1 ? "python3" : supported[0]


          return req.sendOk(

            vali.async(req.body.json, constraints)
            .then(adata=>{

                data = adata

                return readPythonApps(getPythonAppsFilterForWebhosting(req.hosting))
            })
            .then(apps=>{
                // console.log("!!!", req.hosting.template.t_max_number_of_python_apps, apps)
                if(apps.length >= req.hosting.template.t_max_number_of_python_apps)
                   throw new MError("MAX_NUMBER_OF_PYTHON_APPS_REACHED")

                return writePythonAppConfig(req.hosting, data)
            })
            .then(()=>{
                  app.InsertEvent(req, {e_event_type: "python-insert", e_other:true});
            })
          )
      }))
      .get(getWebHostingByIdPythonExpress(function (req, res, next) {

          return readPythonAppsAndSend(getPythonAppsFilterForWebhosting(req.hosting), req, res, next)

      }))
      .delete(common.getWebHostingByIdExpress(function (req, res, next) {
          return deletePythonAppsAndSend(req.hosting.wh_id, null, req)
            .then(()=>{
                  app.InsertEvent(req, {e_event_type: "python-removed", e_other:true});
            })
      }))


   function getAllPythonApps() {
       var apps
          return readPythonApps("*")
            .then(aapps=>{
               apps = aapps

               var ps = []
               apps.forEach(app=>{
                 ps.push(firstline(app.full)
                  .then((result)=>{
                     app.python_app = result
                  })
                  .catch(ex=>{
                    console.error("error reading python app",app.full, ex)
                 }))
               })

               return Promise.all(ps)

            })
            .then(()=>{
               return Promise.all(apps)
            })

   }

   function resolveWebhostings(pyapps) {
      var hostingLib = app.WebhostingLib(app.knex, app)

      var ps = []
      var re = []
      pyapps.forEach(pyapp=>{
         ps.push(
             hostingLib.GetWebhostingById(pyapp.webhosting)
               .then(wh=>{
                  pyapp.hosting = wh
                  re.push(pyapp)
               })
         )

      })
      return Promise.all(ps)
        .then(()=>{
           return Promise.resolve(re)
        })
   }

    router.post("/python/regenerate", function (req, res, next) {
        var all = 0
          return getAllPythonApps()
            .then(pyapps=>{
               return resolveWebhostings(pyapps)
            })
            .then(pyapps=>{
               var ps = []
               pyapps.forEach(pyapp=>{
                 ps.push(
                   writePythonAppConfig(pyapp.hosting, pyapp.python_app)
                   .then(()=>{
                      all++
                   })
                  )
               })

               return Promise.all(ps)
            })
            .then(()=>{
                req.sendResponse({regenerated: all})
            })

      })
    router.get("/python", function (req, res, next) {
          return readPythonAppsAndSend("*", req, res, next)
      })


    var pythonRe = {}
  pythonRe.DeletePythonAppsOfWebhosting = function (webhosting) {
      return deletePythonApps(webhosting.wh_id, null)
  }
   return pythonRe


  function getPythonAppsFilterForWebhosting(webhosting) {
    return webhosting.wh_id+"-*"

  }


  function deletePythonAppsAndSend(webhostingFilter, appFilter, req) {
     return deletePythonApps(webhostingFilter, appFilter)
            .then((deleted)=>{
               req.sendResponse({deleted: deleted})
            })
  }
  function deletePythonApps(webhostingFilter, appFilter) {
          var deleted = 0
          return readPythonApps(webhostingFilter+"-*", appFilter)
            .then(apps=>{
                var ps = []
                apps.forEach(app=>{

                   ps.push(
                      fs.unlinkAsync(app.full)
                      .then(()=>{
                        deleted++
                      })
                      .catch(ex=>{
                         console.error("unable to delete app config: ", app.full, ex)
                      })
                   )
                })

                return Promise.all(ps)
            })
            .then(()=>{
               return Promise.resolve(deleted)
            })

  }

  function readPythonAppsAndSend(filter, req,res,next) {
     return readPythonApps(filter)
       .then(files=>{
          var re = []
          files.forEach(x=>{
             delete x.full
             re.push(x)
          })
          req.sendResponse(re)

       })
  }

  function readPythonApps(filter, appFilter) {
     var find = require('promise-path').find;
     var dir = app.config.get("uwsgi-emperor-directory")
     return find(path.join(dir,'/'+filter+'.ini'))
       .then(files=>{
           // console.log("files", files, filter)
          var re = []
          files.forEach(full=>{
             var m = PathSplitRegexp.exec(full)
             if(!m) return
             var app = {"webhosting":m[1], "python_app_name":m[2], full: full}

             if((appFilter)&&(appFilter != app.python_app_name)) return
             re.push(app)

          })
          return Promise.resolve(re)
       })

  }



   function getWebHostingByIdPythonAppExpress(callback) {
      return getWebHostingByIdPythonExpress(function(req,res,next){

          return readPythonApps(getPythonAppsFilterForWebhosting(req.hosting), req.params.python_app_name)
            .then(apps=>{
               if((!apps)||(apps.length <= 0)) throw new MError("APP_NOT_FOUND")
               var app = apps[0]
               return firstline(app.full)

            })
            .then(result=>{
                 req.python_app = result

                 return callback(req,res,next)

            })


      })
   }

   function getWebHostingByIdPythonExpress(callback) {
      return common.getWebHostingByIdExpress(function(req,res,next){
          if((req.hosting.template)&&(req.hosting.template.t_allowed_dynamic_languages)&&(req.hosting.template.t_allowed_dynamic_languages.indexOf("python") < 0))
              throw new MError("PYTHON_NOT_ALLOWED_FOR_THIS_WEBHOSTING")

          return callback(req,res,next)
      })
   }

  function getConfigFilePath(webhosting, data){
    return path.join( app.config.get("uwsgi-emperor-directory"), webhosting["wh_id"]+"-"+data.python_app_name+".ini")
  }

}
