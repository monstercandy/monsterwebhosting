var lib = module.exports = function(app, router, common) {

    var fs = require("MonsterDotq").fs();

    router.post("/tallyftp", function (req, res, next) {
       const hostingLib = app.WebhostingLib(app.knex, app)
       return req.sendOk(hostingLib.DoSendTalliesToFtp())
    })


  Array({u:"/tallydetails",m:"SetTallyDetails"},{u:"/tallies",m:"SetTallies"}).forEach(q=>{

    router.post(q.u, function (req, res, next) {
       return req.sendOk(
         app.knex.transaction(trx=>{
            var hostingLib = app.WebhostingLib(trx, app)

            return hostingLib.ProcessTalliesForWebhostings(req.body.json, q.m)

         })
       )

    })


  })


  router.get("/bandwidth-limited", function (req, res, next) {
        const hostingLib = app.WebhostingLib(app.knex, app)
        return req.sendPromResultAsIs(hostingLib.GetBandwidthLimitedWebstores())
  });

  Array("web","mail").forEach(cat=>{
    router.post("/:webhosting_id/szar/checkpoints/"+cat, common.getWebHostingByIdExpress(function (req, res, next) {
          return req.sendPromResultAsIs(req.hosting.SzarCheckpoints(cat))
    }))

    router.post("/:webhosting_id/szar/backup/"+cat, common.getWebHostingByIdExpress(function (req, res, next) {
          return req.sendTask(req.hosting.SzarDoBackup(cat, null, req))
    }))
    router.post("/:webhosting_id/szar/restore/"+cat, common.getWebHostingByIdExpress(function (req, res, next) {
          return req.sendTask(req.hosting.SzarDoRestore(cat, req.body.json, req))
    }))
    router.post("/:webhosting_id/szar/jump/"+cat, common.getWebHostingByIdExpress(function (req, res, next) {
         return req.sendResponse(req.hosting.SzarJump(cat))
    }))

    router.post("/szar/backup/"+cat, function (req, res, next) {

        var hostingLib = app.WebhostingLib(app.knex, app)

        return common.emitterChainBeginner(req, {dontLog_stdout: true})
            .then(gemitter=>{
              return common.emitterChainFinisher(gemitter,
                  hostingLib.DoSzarBackupAll(gemitter),
                  "backup "+cat
              )
        })

    })

  })


    router.post("/:webhosting_id/tallydetails", common.getWebHostingByIdExpress(function (req, res, next) {
           return req.sendOk(req.hosting.SetTallyDetails(req.body.json))
      }))

    router.post("/:webhosting_id/tallies", common.getWebHostingByIdExpress(function (req, res, next) {
           return req.sendOk(req.hosting.SetTallies(req.body.json))
      }))

    router.post("/:webhosting_id/tallywarning", common.getWebHostingByIdExpress(function (req, res, next) {
           return req.sendOk(req.hosting.SendTallyWarning())
      }))

   function walkThrough(req, storeIt){
       var stats
        return common.emitterChainBeginner(req, {dontLog_stdout: true})
            .then(gemitter=>{
              return common.emitterChainFinisher(gemitter,
                  req.hosting.Walk(gemitter)
                     .then((stats)=>{
                        if(!storeIt) return Promise.resolve(stats)

                        gemitter.send_stdout_ln("Storing results...")
                        return req.hosting.SetTallyDetails(stats, true)
                     })
                   ,
                  "walking"
              )
        })
   }

    router.post("/:webhosting_id/walk", common.getWebHostingByIdExpress(function (req, res, next) {
       return walkThrough(req, false)
    }))

    router.post("/:webhosting_id/walk-and-store", common.getWebHostingByIdExpress(function (req, res, next) {
       return walkThrough(req, true)

    }))


    router.route("/:webhosting_id")
      .post(common.getWebHostingTrxByIdExpress(function (req, res, next) {
           return req.sendOk(
             req.hosting.Change(req.body.json)
              .then(()=>{
                  app.InsertEvent(req, {e_event_type: "webhosting-change", e_other:true});
              })
           )
      }))
      .get(common.getWebHostingByIdExpress(function (req, res, next) {
           req.sendResponse(req.hosting.Cleanup())
      }))
      .delete(common.getWebHostingByIdExpress(function (req, res, next) {
          var phplib = require("lib-phpfpm.js")(app, req.hosting)

          return common.emitterChainBeginner(req)
            .then(gemitter=>{
              return common.emitterChainFinisher(gemitter,
                  common.runHooksViaEmitter(gemitter, [req.hosting], "uninit", req.body.json)
                    .then(()=>{
                      app.InsertEvent(req, {e_event_type: "webhosting-remove", e_other:true});

                      return common.runHooksViaEmitter(gemitter, [req.hosting], "remove", req.body.json)
                    }),
                  "removing a webhosting: "+req.params.webhosting_id
              )

            })

      }))

    router.route("/")
      .get(common.getWebhostingExpress(function(req,res,next){
          return req.hostingLib.GetAllWebhostings()
            .then(d=>{
              req.sendResponse(d.Cleanup())
            })

      }))
      .put(common.getWebhostingExpress(function(req,res,next){
          var hosting
          return req.hostingLib.Insert(req.body.json, req)
            .then(id=>{
                return req.hostingLib.GetWebhostingById(id)
            })
            .then(aHosting=>{
                hosting = aHosting

                return common.emitterChainBeginner()
            })
            .then(gemitter=>{

                return common.emitterChainFinisher(gemitter,
                    common.runHooksViaEmitter(gemitter, [hosting], "create", req.body.json)
                    .then(()=>{
                        return common.runHooksViaEmitter(gemitter, [hosting], "init", req.body.json)
                    }),
                  "creating a webhosting: "+hosting.wh_id
                )

            })
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "webhosting-create", e_other:true});

                // TODO: make the response prompt and report the task id where readers can watch the async process
                req.sendResponse({wh_id:hosting.wh_id, template: hosting.template})
            })
      }))
}
