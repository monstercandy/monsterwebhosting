module.exports = function(app) {

  var router = app.ExpressPromiseRouter();

  router.post("/szar/checkpoints", function(req,res,next){
         return req.sendPromResultAsIs(
            app.SzarLib.SzarCheckpoints()
         )

     })

  // TODO: add support for restoring stuff under /var/lib/monster

    router.post("/szar/backup", function (req, res, next) {
          var gemitter = app.commander.EventEmitter()
          return req.sendTask(
          	gemitter.spawn({"omitAggregatedOutput":true,logToError:true})
            .then(x=>{

                app.SzarLib.SzarDoBackup(gemitter, req);

            	return x;
            })
          )

      })

  return router




}
