var lib = module.exports = function(app, router, common) {

    router.get("/supported/storages",function (req, res, next) {
           req.sendResponse(app.storages)
      })

    router.get("/supported/php",function (req, res, next) {
           //, [{"php_version":"7.0", "basedir": "/usr/local/php70", "conf_file": re.php_fpm_conf_file}]) // conf_file is for testing only
           var re = app.getPhpVersionsArr();

           req.sendResponse(re)
      })

    router.get("/supported/python", function (req, res, next) {
           req.sendResponse(app.getSupportedPythonMajorVersions())
      })


  router.post("/lookup/id", function(req,res,next){
        var vali = require("MonsterValidators").ValidateJs()

          return vali.async(req.body.json, {
             "ids": {presence: true, isArray: true},
          })
          .then(function(d){
             questionMarks = d.ids.map(x => { return "?" }).join(",")
             // console.log("qmarks", questionMarks)
             return app.knex("webhostings").select("wh_id","wh_name","wh_user_id").whereRaw("wh_id IN ("+questionMarks+")", d.ids)
          })
          .then(m=>{
              var re = {}
              m.forEach(row=>{
                 re[row.wh_id] =  {name: row.wh_name, user: row.wh_user_id}
              })
              req.result = re
              next()
          })
  })


}
