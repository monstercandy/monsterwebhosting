(function(){

/*
// moment(Number);
this is what we need to parse:

[13-Oct-2017 18:10:33]  [pool p16509] pid 8216
script_filename = /bm.monstermedia.hu/pages/x.php
[0x00007f3b9fc150f0] sleep() /bm.monstermedia.hu/pages/x.php:2
*/

    const Tail = require("MonsterTail").Tail;
    const moment = require("MonsterMoment");

    const databaseSelection = /^use (.+);/;
    const poolRegexp = /^\[[0-9]+-[A-Za-z]{3}[^\]]+\]\s+\[pool p([0-9]+)\]/;
    const scriptFilenameRegexp = /^script_filename = (.+)/;

    var lib = module.exports = function(options) {

       if(!options.onEvent) throw new Error("onEvent not provided");

       var re = {};

       var tail;

       var latestEvent = {};
       var dispatchLastTimer;

       re.unwatch = function(){
         if(!tail) return;
         tail.unwatch();
         tail = null;
       }

       re.processLine = function(linesStr) {
          var lines = linesStr.split("\n");
          lines.forEach(line=>{
              if(!line) return dispatchEvent();

              var m = poolRegexp.exec(line);
              if(m) {
                 latestEvent.sl_timestamp = moment.now();
                 latestEvent.sl_webhosting = m[1];
                 return;
              }

              m = scriptFilenameRegexp.exec(line);
              if(m) {
                 latestEvent.sl_path = m[1];
                 return;
              }

              if(!latestEvent.sl_dump)
                latestEvent.sl_dump = "";
              latestEvent.sl_dump += line+"\n";

              if(dispatchLastTimer)
                clearTimeout(dispatchLastTimer);

              dispatchLastTimer = setTimeout(function(){
                dispatchLastTimer = null;
                dispatchEvent();
              }, 1000);

          })
       }


       re.setupTail = function(){
          if(!options.filename) return;

          re.unwatch();

          tail = Tail.robust(options.filename, {deletedDelayMs: 30000, silent: true});
          tail.on("line", re.processLine);
       }

       re.setupTail();

       return re;


       function dispatchEvent(){
          if(Object.keys(latestEvent).length) {
            options.onEvent(latestEvent);
            latestEvent = {};

          }
       }


    }

})()
