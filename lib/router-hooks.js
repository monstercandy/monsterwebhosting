var lib = module.exports = function(app, router, common){


    router.post("/hook/:hook", common.getWebhostingExpress(function(req,res,next){

       return req.hostingLib.GetAllWebhostings()
        .then(webhostings =>{
           app.InsertEvent(req, {e_event_type: "hook-run", e_other:true});
           return common.runHooks(webhostings, req.params.hook, req.body.json, req)
        })


    }))

    router.post("/:webhosting_id/hook/:hook", common.getWebHostingByIdExpress(function(req,res,next){
    	   app.InsertEvent(req, {e_event_type: "hook-run", e_other:true});
         return common.runHooks([req.hosting], req.params.hook, req.body.json, req)

    }))

}