#!/bin/bash

set -e

DIR=$(dirname $0)

unshare -m "$DIR/requirements-unshared.sh" $@
