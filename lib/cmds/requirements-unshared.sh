#!/bin/bash

set -e

DIR=$(dirname $0)

STORAGE_ID=$1
PYTHON_VER=$2
REMOVE_FIRST=$3
DO_UPGRADE=$4
if [ -z "$PYTHON_VER" ]; then
  echo "Usage: $0 storage_id (python2|python3) [remove_first] [do_upgrade]"
  exit 1
fi




DISTRO_ROOT="/lxc/jessie-uwsgi/distros/jessie-root-$PYTHON_VER"

STORAGE_TEMP_DIR="/tmp/$STORAGE_ID"

function cleanup() {
  rm -rf "$STORAGE_TEMP_DIR"
}

trap "{ cleanup;  }" EXIT

mkdir "$STORAGE_TEMP_DIR"
chown $STORAGE_ID "$STORAGE_TEMP_DIR"

STORAGE_PASSWD_TEMP="$STORAGE_TEMP_DIR/$STORAGE_ID"

echo "nobody:x:$STORAGE_ID:$STORAGE_ID:nobody:/web/wsgi-root:/usr/sbin/nologin" >"$STORAGE_PASSWD_TEMP"

mount --bind /web/w3/$STORAGE_ID-* "$DISTRO_ROOT/web"
mount --bind "$STORAGE_TEMP_DIR" "$DISTRO_ROOT/tmp"
mount --bind "$STORAGE_PASSWD_TEMP" "$DISTRO_ROOT/etc/passwd"
mount --bind /etc/resolv.conf "$DISTRO_ROOT/etc/resolv.conf"


export HOME=/web/wsgi-root

#ulimit -a
CLEANUP_STR=""
if [ "1" == "$REMOVE_FIRST" ]; then
  echo "Cleaning up first"
  CLEANUP_STR="rm -rf ~/.local"
fi

UPGRADE_STR=""
if [ "1" == "$DO_UPGRADE" ]; then
  echo "Using the upgrade flag"
  UPGRADE_STR="--upgrade"
fi

chroot "--userspec=$STORAGE_ID:65534" "$DISTRO_ROOT" /bin/bash <<EOF
$CLEANUP_STR
REQ_LINES=$(cat ~/requirements.txt | wc -l)
if [ "$REQ_LINES" -gt "$MAX_REQ_LINES" ]; then
  echo "Too many requirements!"
  exit 1
else
  pip install --user $UPGRADE_STR -r ~/requirements.txt
fi
EOF


umount "$DISTRO_ROOT/tmp"
umount "$DISTRO_ROOT/web"
#umount "$DISTRO_ROOT/etc/resolv.conf"
#umount "$DISTRO_ROOT/etc/passwd"

