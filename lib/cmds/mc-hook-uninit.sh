#!/bin/bash

set -e

. /opt/MonsterWebhosting/lib/cmds/mc-hook-init-uninit-common.sh

if mountpoint "$WS_PHP_ENVDIR"; then
  umount $WS_PHP_ENVDIR
fi

if mountpoint "$WS_MAIL_DIR"; then
  umount $WS_MAIL_DIR
fi
