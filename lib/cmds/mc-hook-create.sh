#!/bin/bash

FULLPATH=$1
WEBSTORE_ID=$2

set -e

cd "$FULLPATH"
ln -s /var/lib/php/env/dev dev
ln -s /var/lib/php/env/etc etc
ln -s /var/lib/php/env/usr usr
