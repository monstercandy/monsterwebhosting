#!/usr/bin/perl

# the script depends the forkstat utility
# usage:
#  stdbuf -o 0 forkstat -e exec,fork -s | ./cpu-governor.pl 80 16000 20000
# or maybe via the expect-dev package:
#  expect_unbuffer forkstat -e exec,fork -s | ./cpu-governor.pl 80 16000 20000
#
# use the following command line to print debug messages of the script:
# perl -MSmart::Comments cpu-governor.pl 

use strict;
use warnings;
use Time::HiRes qw( gettimeofday );
use Data::Dumper;
use IO::Select;

use constant SHOW_STATS_INTERVAL => 3600; # in seconds
use constant PRIO_PROCESS => 0;
use constant PRIO_PGRP => 1;
use constant PRIO_USER => 2;
use constant MAX_PRIORITY => -20;
use constant MIN_PRIORITY => 20;
use constant PROC => "/proc";
use constant HZ => 100.0;
use constant ALFA => 0.08;
use constant MIN_DT => 20;
use constant SLEEP => 2.5;
use constant PRIO_STEP_UP => 4;
use constant PRIO_STEP_DOWN => 1;
use constant DEMO_MODE => 0;
use constant AGRESSIVE => 1; # in agressive mode we do renice as penalty as soon as violation is detected
                             # in non agressive mode several occasions of violations are needed before penalty
use constant REBUILD_PROCESS_LIST => 0; # we dont rebuild it manually but through forkstat

my $NCPU = get_ncpu();

my $per_minute_limit = $ARGV[0] ? int($ARGV[0]) : -1;
my $min_uid = $ARGV[1];
my $max_uid = $ARGV[2];
my $one_process_pid = $ARGV[3];

die "Usage: expect_unbuffer forkstat -e exec,fork -s | $0 max_cpu_limit min_uid max_uid [one_process_pid_only]" if(($per_minute_limit <= 0)||($per_minute_limit > $NCPU*100)||(!$min_uid)||(!$max_uid)||($max_uid<$min_uid)||($min_uid < 0));

my $limit = $per_minute_limit / 100.0;

### limit: $limit


my $last_update = {"tv_sec"=>0, "tv_usec"=>0};
my %monitored_processes;

my $s = IO::Select->new();
$s->add(*STDIN);
### forkstat select started...

my $counter = 0;
my $stat_counter = 0;
my $last_process_list = 0;
while(1)
{
   rebuild_process_list() if((!$counter)||((REBUILD_PROCESS_LIST)&&($counter % REBUILD_PROCESS_LIST  == 0)));


#  print Dumper(\%monitored_processes);

   update_process_group();



        # estimate how much the controlled processes are using the cpu in the working interval
        my $headersSent = 0;
        for my $uid (keys %monitored_processes) {


            my $pcpu = -1;
            for my $pid (keys %{$monitored_processes{$uid}{"procs"}}) {
                next if ($monitored_processes{$uid}{"procs"}{$pid}{'cpu_usage'} < 0);
                $pcpu = 0 if ($pcpu < 0);
                $pcpu += $monitored_processes{$uid}{"procs"}{$pid}{'cpu_usage'};
                                
                                ### PROC: "$uid / $pid ". $monitored_processes{$uid}{"procs"}{$pid}{'cpu_usage'}
            }

            if($pcpu != -1) {
               if(!$headersSent) {
                   print localtime()." STATS:\n";
                   $headersSent = 1;
               }
               
               print "TOP: $uid: $pcpu\n" if($pcpu != -1);
            }

            # implement exceed check logic
            my $new_nice;
            if($pcpu >= $limit) {
              ### limit exceeded...
                          if($monitored_processes{$uid}{"nice"} + PRIO_STEP_UP <= MIN_PRIORITY) {
                               ### we still have a way to slower the process...
                                   $monitored_processes{$uid}{"score"}+= 1;
                                   if((AGRESSIVE)||($monitored_processes{$uid}{"score"} > $monitored_processes{$uid}{"nice"}))
                                   {
                                          $new_nice = $monitored_processes{$uid}{"nice"} + PRIO_STEP_UP;
                                   }
                           
                           }

            } elsif($monitored_processes{$uid}{"nice"} - PRIO_STEP_DOWN >= 0) {
               ### limit not exceeded and process priority was lowered...
               $monitored_processes{$uid}{"score"}-- ;
               if((AGRESSIVE)||(-1*$monitored_processes{$uid}{"score"} > $monitored_processes{$uid}{"nice"}))
               {
                  $new_nice = $monitored_processes{$uid}{"nice"} - PRIO_STEP_DOWN;
               }

            }

            if(defined($new_nice)) {
               ### renicing user group: "$uid from ".$monitored_processes{$uid}{"nice"}." to $new_nice"
                           print "renicing user group: $uid from ".$monitored_processes{$uid}{"nice"}." to $new_nice\n";
               $monitored_processes{$uid}{"nice"} = $new_nice;
               $monitored_processes{$uid}{"score"} = 0;
                           
               setpriority(PRIO_USER, $uid, $monitored_processes{$uid}{"nice"}) if(!DEMO_MODE);
            }

            $monitored_processes{$uid}{"stat"} += $monitored_processes{$uid}{"nice"};

                        print "\n";

        }
        print "\n";

### <now> forkstat part begin, entering sleep...
   my $before = mygettimeofday();
   my $spent = 0;
   my $asleep = SLEEP;
   while($spent < (SLEEP*0.95)) {
       my @ready = $s->can_read($asleep);
       if(scalar @ready) {
          while($s->can_read(0)) {
             my $line = <STDIN>;

             ### forkstat: $line
             my @r = split /\s+/, $line;
                         next if(!$r[2]);
             new_process_event($r[2], 0, 1) if($r[2] =~ /^\d+$/);
          }
       }

       my $now = mygettimeofday();
       $spent = timediff($now, $before) / 1000000;
           $asleep = SLEEP - $spent;
       ### sleeping: "spent $spent, asleep $asleep"
   }
### <now> forkstat part ready, measuring activities

   $counter++;
   $stat_counter+= SLEEP;

  if((SHOW_STATS_INTERVAL)&&($stat_counter >= SHOW_STATS_INTERVAL)) {
     $stat_counter=0;
     for my $uid (keys %monitored_processes) {
        my $statr = \$monitored_processes{$uid}{"stat"};
        if($$statr) {
           print "$uid: $$statr\n";
           $$statr = 0;
        }
     }
  }
}

sub get_ncpu {
    my $rc = open my $CPU, PROC."/cpuinfo";
    if(!$rc) {
      warn "Can't open cpuinfo: $!\n";
      return 1;
    }
    my $ncpus = scalar (map /^processor/, <$CPU>) ;
    close $CPU;
    return $ncpus;
}




sub get_uid_of_process {
  my $pid = shift;

  my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
           $atime,$mtime,$ctime,$blksize,$blocks)
               = stat(PROC."/$pid");
    return $uid || -1;
}

sub new_process_event {
  my $pid = shift;
  my $auid = shift || 0;
  my $update = shift || 0;
  if(!$auid) {
    $auid = get_uid_of_process($pid);
  }
  return if($auid <= 0);

  return if($auid < $min_uid);
  return if($auid > $max_uid);

  if(!defined($monitored_processes{$auid}{"procs"}{$pid})) {
     ### New process: "$auid / $pid"
         update_process($auid, $pid, 0, {"tv_sec"=>0, "tv_usec"=>0}) if($update);
  }

  $monitored_processes{$auid}{"procs"}{$pid}{"running"} = 1;
  $monitored_processes{$auid}{"nice"} = 0 if(!defined($monitored_processes{$auid}{"nice"}));
  $monitored_processes{$auid}{"score"} = 0 if(!defined($monitored_processes{$auid}{"score"}));
  $monitored_processes{$auid}{"stat"} = 0 if(!defined($monitored_processes{$auid}{"stat"}));


  if($monitored_processes{$auid}{"nice"}) {
     ### immediate renicing of new process: "$pid to ". $monitored_processes{$auid}{"nice"}
     setpriority(PRIO_PROCESS, $pid, $monitored_processes{$auid}{"nice"});
  }
}

sub rebuild_process_list {

### rebuilding process list...

  for my $uid (keys %monitored_processes) {
     for my $pid (keys %{$monitored_processes{$uid}{"procs"}}) {
       $monitored_processes{$uid}{"procs"}{$pid}{"running"} = 0;
     }
  }

  opendir(my $D, PROC) or die "Cant read proc? : $!";
  while(my $pid = readdir($D)) {
     next if($pid !~ /^\d+$/);
     next if(!-d PROC.'/'.$pid);
     next if(($one_process_pid)&&($pid != $one_process_pid));

     new_process_event($pid);

  }
  closedir($D);

  for my $uid (keys %monitored_processes) {
     for my $pid (keys %{$monitored_processes{$uid}{"procs"}}) {
       if(!$monitored_processes{$uid}{"procs"}{$pid}{"running"}) {
          ### Process not running anymore (build): "$uid / $pid"
          delete $monitored_processes{$uid}{"procs"}{$pid};
          # print Dumper(\%monitored_processes);
       }
     }
  }

### %monitored_processes

}

sub timediff {
  my ($t1, $t2) = @_;

  return ($t1->{'tv_sec'} - $t2->{'tv_sec'}) * 1000000 + ($t1->{'tv_usec'} - $t2->{'tv_usec'});
}

sub mygettimeofday {
   my ($seconds, $microseconds) = gettimeofday();
   my %re;
   $re{'tv_sec'} = $seconds;
   $re{'tv_usec'} = $microseconds;
   return \%re;
}

sub update_process {
  my $uid = shift;
  my $pid = shift;
  my $now = shift || mygettimeofday();
  my $local_update = shift || 0;
  
  
  my $a_last_update = $last_update;
  $a_last_update = $local_update if($local_update);
  if(defined($monitored_processes{$uid}{"procs"}{$pid}{"last_update"})) {
    $a_last_update = $monitored_processes{$uid}{"procs"}{$pid}{"last_update"};
        undef($monitored_processes{$uid}{"procs"}{$pid}{"last_update"});
  ### using process local last update for calculating delta time: $a_last_update
  }
  
  
  my $dt = timediff($now, $a_last_update) / 1000;
  ### dt: $uid / $pid / $dt  

          my $PROC;
          if(!open($PROC, "<".PROC."/$pid/stat")) {
              ### Process not running anymore (update): "$pid / $uid"
              delete $monitored_processes{$uid}{"procs"}{$pid};
              # print Dumper(\%monitored_processes);
              return;
          }

          my $l = <$PROC>;
          close($PROC);

          my @ll = split(" ", $l);
          # print Dumper(\@ll);

          # (14) utime  %lu, (15) stime  %lu
          my $new_cputime = int((int($ll[13]) + int($ll[14])) * 1000 / HZ);

          if(!defined($monitored_processes{$uid}{"procs"}{$pid}{"cputime"})) {
             $monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"} = -1;
          }
          else
          {
            if ($dt >= MIN_DT) {
                my $sample = 1.0 * ($new_cputime - $monitored_processes{$uid}{"procs"}{$pid}{"cputime"}) / $dt;
                ### update sample for: "$pid $sample"
                if($monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"} == -1) {
                  $monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"} = $sample;
                }else{
                  $monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"} = (1.0 - ALFA) * $monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"} + ALFA * $sample;
                }
            }
          }

          my $new_cpuusage = $monitored_processes{$uid}{"procs"}{$pid}{"cpu_usage"};
          $monitored_processes{$uid}{"procs"}{$pid}{"cputime"} = $new_cputime;
          ### cpu time for: "$pid is $new_cputime ($new_cpuusage)"  
  
  
  if($local_update) {
### updating process local last update to: $now
    $monitored_processes{$uid}{"procs"}{$pid}{"last_update"} = $now;
  } 

}

sub update_process_group {
  my $now = mygettimeofday();
  for my $uid (keys %monitored_processes) {
     for my $pid (keys %{$monitored_processes{$uid}{"procs"}}) {
          update_process($uid, $pid, $now);
     }
  }

### updating global last update to: $now
    $last_update = $now;
}
