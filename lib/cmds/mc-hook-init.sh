#!/bin/bash

set -e

. /opt/MonsterWebhosting/lib/cmds/mc-hook-init-uninit-common.sh

if ! mountpoint "$WS_PHP_ENVDIR"; then
  mount --bind $ROOT_PHP_ENVDIR $WS_PHP_ENVDIR
fi

if ! mountpoint "$WS_MAIL_DIR"; then
  mount --bind $ROOT_MAILS_DIR $WS_MAIL_DIR
fi
