module.exports = function(app) {

    var router = app.ExpressPromiseRouter({mergeParams: true});


  router.use(function(req,res,next){
     if(!req.body.json) req.body.json = {};
     if(req.params.webhosting)
        req.body.json.sl_webhosting = req.params.webhosting;
     next();
  });


  router.post("/search", function(req,res,next){
          return req.sendPromResultAsIs(
              app.getSlowlog().Query(req.body.json)
          )

     })

  router.post("/count", function(req,res,next){
          return req.sendPromResultAsIs(
             app.getSlowlog().Count(req.body.json)
          )
     })

  router.post("/reopen", function(req,res,next){
          return req.sendOk(
             app.getSlowlog().setupSlowlogWatchers()
          )
     })

  return router





}
