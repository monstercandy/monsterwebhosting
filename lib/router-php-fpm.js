var lib = module.exports = function(app, router, common) {

   function savePhpFpmConfigs(webhostings, req) {
      var ps = []
      webhostings.forEach(wh=>{
          ps.push(wh.SavePhpFpmConfig())
      })
      return Promise.all(ps)
         .then(()=>{
             app.InsertEvent(req, {e_event_type: "php-fpm-config", e_other:true});
         })

   }

    router.post("/php/fpm/config", common.getWebhostingExpress(function(req,res,next){
       return  req.sendOk(req.hostingLib.GetAllWebhostings()
         .then(webhostings =>{
            return savePhpFpmConfigs(webhostings, req)
         })
       )
    }))

    function rehashPhpFpms(req) {
       const phpfpmlib = require("lib-phpfpm.js")
       var PhpFpm = phpfpmlib(app)
       return PhpFpm.RehashPhpFpm(req.params.version)
         .then((r)=>{
             app.InsertEvent(req, {e_event_type: "php-fpm-rehash", e_other:true});
             return req.sendResponse(r);
         })

    }

    router.post("/php/fpm/rehash", function(req,res,next){
       return rehashPhpFpms(req)
    })

    router.post("/php/fpm/rehash/:version", function(req,res,next){
       return rehashPhpFpms(req)
    })

    router.post("/:webhosting_id/php/fpm/config", common.getWebHostingByIdExpress(function(req,res,next){
       return  req.sendOk(savePhpFpmConfigs([req.hosting], req))
    }))

    router.get("/:webhosting_id/php/fpm/config", common.getWebHostingByIdExpress(function(req,res,next){
       return  req.sendPromResultAsIs(req.hosting.ReadPhpFpmConfig())
    }))

}
