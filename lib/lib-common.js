var lib = module.exports = function(app) {

   const MError = require("MonsterError")

   var re = {

        getWebhostingExpress: function(cb) {
           return function(req,res,next){
               req.hostingLib = app.WebhostingLib(app.knex, app)
               return cb(req,res,next)
           }
        },

        getWebhostingTrxExpress: function(cb) {
           return function(req,res,next){
               return app.knex.transaction(function(trx){
                   req.hostingLib = app.WebhostingLib(trx, app)
                   return cb(req,res,next)
               })
           }
        },

        getWebHostingByIdExpress: function(cb) {
            return re.getWebhostingExpress(function(req,res,next){

                return req.hostingLib.GetWebhostingById(req.params.webhosting_id)
                  .then(hosting=>{
                     req.hosting = hosting
                     return cb(req,res,next)
                  })
            })
        },

        getWebHostingTrxByIdExpress: function(cb) {
            return re.getWebhostingTrxExpress(function(req,res,next){

                return req.hostingLib.GetWebhostingById(req.params.webhosting_id)
                  .then(hosting=>{
                     req.hosting = hosting
                     return cb(req,res,next)
                  })
            })
        },

        verifyHook: function(webhostings, hook_name) {
           var hooks = (app.config.get("hooks") || {})
           var commands_before = hooks[hook_name] || hooks[hook_name+"-before"]
           var commands_after = hooks[hook_name+"-after"]
           if((!commands_before) && (!commands_after) && ((!webhostings[0].hooks) || (!webhostings[0].hooks[hook_name])))
             throw new MError("HOOK_NOT_CONFIGURED", null, hook_name)

           return {hook_name: hook_name, commands_before: commands_before, commands_after: commands_after}
        },

        runHooksViaEmitter: function(gemitter, webhostings, hooksOrHookName, extraParams) {

           var hooks = hooksOrHookName
           if(typeof hooks != "object")
              hooks = re.verifyHook(webhostings, hooksOrHookName)

           var promises = []

           gemitter.send_stdout("Starting "+hooks.hook_name+"...\n")

           const dotq = require("MonsterDotq");
           return dotq.linearMap({
              array: webhostings,
              catch: ex=>{
                 console.error("error while executing hook", hooksOrHookName, ex);

                 // we rethrow this exception for non-batch operations only:
                 if(webhostings.length == 1)
                    throw ex;
              },
              action: wh=>{

               var baseProm
               if(!hooks.commands_before)
                 baseProm = Promise.resolve()

               if(hooks.commands_before)
                 baseProm = app.commander.spawn({emitter: gemitter, omitAggregatedOutput:true, chain:hooks.commands_before}, wh)
                 .then(h=>{
                    gemitter.send_stdout("Running before hooks of category '"+hooks.hook_name+"'...\n")

                    promises.push(h.executionPromise)

                    return h.executionPromise
                 })

               return baseProm
                 .then(()=>{
                    if((!wh.hooks)||(!wh.hooks[hooks.hook_name])) return Promise.resolve()

                    gemitter.send_stdout("Intermediate (in javascript) hooks of category '"+hooks.hook_name+"'...\n")
                    return wh.hooks[hooks.hook_name](extraParams, gemitter)
                 })
                 .then(()=>{
                    if(!hooks.commands_after) return Promise.resolve()

                    gemitter.send_stdout("Running after hooks of category '"+hooks.hook_name+"'...\n")

                    return app.commander.spawn({emitter: gemitter, omitAggregatedOutput:true, chain:hooks.commands_after}, wh)
                       .then(h=>{
                          promises.push(h.executionPromise)

                          return h.executionPromise
                       })
                 })

           }})

        },

        runHooks: function(webhostings, hook_name, extraParams, req) {

          var hooks
          return Promise.resolve()
            .then(()=>{
                hooks = re.verifyHook(webhostings, hook_name)
                return re.emitterChainBeginner(req)
            })
            .then(gemitter=>{
               return re.emitterChainFinisher(
                 gemitter,
                 re.runHooksViaEmitter(gemitter, webhostings, hooks, extraParams)
               )
            })

       },

       emitterChainBeginner: function(req, extraOptions) {
          var gemitter = app.commander.EventEmitter()
          return gemitter.spawn(extend({"omitAggregatedOutput":true,logToError:true}, extraOptions))
            .then(x=>{
                if(req)
                   req.sendResponse({id:x.id})
                gemitter.id = x.id

                x.executionPromise.catch(ex=>{
                    console.error("Error while executing a hook via eventemitter/commander", ex);
                })

                return Promise.resolve(gemitter)
            })
       },

       emitterChainFinisher: function(gemitter, prom, eventDescription){
            return prom
              .then(() =>{
                 console.log("The hook chain has finished", eventDescription);
                 gemitter.send_stdout("The hook chain has finished\n")
                 gemitter.close() // everything was cool
              })
              .catch(ex=>{
                 console.error("Error during hook:", eventDescription, ex)

                 if(eventDescription)
                   app.InsertException(null, ex, "webhosting action failed: "+eventDescription);

                 gemitter.close(1)
                 throw ex
              })
       }

    }

    return re
}
