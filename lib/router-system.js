var lib = module.exports = function(app, router, common) {

  const dotq = require("MonsterDotq");
  const fs = dotq.fs();


  var router = app.ExpressPromiseRouter();

  router.get("/bin/df", function(req,res,next){
        return req.sendPromResultAsIs(
            run("/bin/df", ["-m"])
        )
     })


  router.get("/bin/smartctl/:disk", function(req,res,next){
        return req.sendPromResultAsIs(
            run("/usr/sbin/smartctl", ["-a", "/dev/"+req.params.disk])
        )
  })

  router.get("/proc/partitions", function(req,res,next){
      return req.sendPromResultAsIs(
        read("/proc/partitions")
        .then(c =>{
           c.disks = lib.parsePartitions(c.output);
           return c;
        })
      );
  })

  Array("mdstat", "uptime", "vmstat", "meminfo", "misc", "version", "swaps", "stat").forEach(cat =>{
      router.get("/proc/"+cat, function(req,res,next){
          return req.sendPromResultAsIs(read("/proc/"+cat));
      })
  })


  return router

  function read(file) {
     return fs.readFileAsync(file, "utf-8")
       .catch(x=>{
          console.error("Unable to read file", file, x);
          if(x.code == "ENOENT") return "[ not found ]";
       })
       .then(c=>{
          return {output: c};
       })
  }

  function run(cmd, args){
      return app.commander.spawn({omitControlMessages: true, dontLog_stdout: true, chain: {executable: cmd, args: args}})
         .then(h=>{
            return h.executionPromise;
         })
         .catch(x =>{
            return {output: "Error ("+x.code+"):\n\n"+x.output}
         })
         .then(x =>{
            return {output: x.output.toString()};
         })
  }

}

lib.parsePartitions = function(p){
  // console.log("got", p)
  var re = [];
  var m;
  var myRe = /\s+[0-9]+\s+(sd[a-z])$/gm; // 
  while (m = myRe.exec(p)) {
    re.push(m[1])
  };
  return re;
}
