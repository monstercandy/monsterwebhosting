
exports.up = function(knex, Promise) {

    return knex.raw("SELECT seq FROM SQLITE_SEQUENCE WHERE name = 'webhostings'")
          .then(n=>{
              if(n.length <= 0)
                  return knex.raw("INSERT INTO SQLITE_SEQUENCE (name, seq) VALUES('webhostings', 10000)").return();
          })


};


exports.down = function(knex, Promise) {
    return Promise.resolve()
};
