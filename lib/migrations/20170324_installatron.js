
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.json('wh_installatron_server').notNullable().defaultTo("");

        }),


    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_installatron_server");
        }),      
    ])
    
};
