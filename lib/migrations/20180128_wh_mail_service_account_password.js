
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.string('wh_mail_service_account_password').notNullable().defaultTo("");

        }),


    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_mail_service_account_password");
        }),      
    ])
    
};
