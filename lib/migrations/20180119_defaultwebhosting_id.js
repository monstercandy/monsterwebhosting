
exports.up = function(knex, Promise) {

    return knex.raw("SELECT seq FROM SQLITE_SEQUENCE WHERE name = 'webhostings'")
          .then(n=>{
              if((n.length != 1)||(n[0].seq!=10000)) return;

              return knex.raw("DELETE FROM SQLITE_SEQUENCE WHERE name='webhostings'").return()
                .then(()=>{
                	return knex.raw("INSERT INTO SQLITE_SEQUENCE (name, seq) VALUES('webhostings', 11000)").return()
                })
          })


};


exports.down = function(knex, Promise) {
    return Promise.resolve()
};
