
exports.up = function(knex, Promise) {

    return Promise.all([
        /*
            th_active_from | th_active_to

            th_php_fpm_conf_extra_line | th_max_execution

            th_firewall | th_firewall_ts
        */

         knex.schema.createTable('webhostings', function(table) {
            table.increments('wh_id').primary();

            table.uuid('wh_user_id');

            table.string('wh_name').notNullable();
            table.string('wh_storage').notNullable();
            table.string('wh_secretcode').notNullable();
            table.string('wh_template').notNullable();
            table.string('wh_template_override').notNullable().defaultTo("");

            table.string('wh_php_version').notNullable();

            table.boolean('wh_is_expired').notNullable();
            table.boolean('wh_is_readonly').notNullable();

            table.boolean('wh_ftp_accepted_from_latest_admin_ip_only').notNullable();
            table.boolean('wh_ftp_accepted_from_foreign_ip').notNullable();
            table.boolean('wh_notify_script_differences').notNullable();
            table.boolean('wh_notify_foreign_ftp_login_events').notNullable();

            table.decimal('wh_tally_web_storage_mb').notNullable();
            table.decimal('wh_tally_mail_storage_mb').notNullable();
            table.decimal('wh_tally_db_storage_mb').notNullable();            
            
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),


         knex.schema.createTable('webhostingdomains', function(table) {
            table.increments('wd_id').primary();
            table.integer('wd_webhosting')
                 .references('wh_id')
                 .inTable('webhostings');
            table.string('wd_domain_canonical_name').notNullable();
            table.string('wd_domain_name').notNullable();
         })

    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('webhostings'),
        knex.schema.dropTable('webhostingdomains'),
    ])
	
};
