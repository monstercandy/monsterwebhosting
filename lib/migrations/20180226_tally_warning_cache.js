
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.decimal('wh_tally_sum_storage_mb_last').notNullable().defaultTo(0);

        }),


    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_tally_sum_storage_mb_last");
        }),
    ])
    
};
