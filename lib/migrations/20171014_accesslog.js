
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.createTable('accesslog', function(table) {
            table.increments('al_id').primary();

            table.integer('al_webhosting')
                 .references('al_id')
                 .inTable('webhostings');

            table.string("al_vhost").notNullable();

            table.integer("al_stat_user").notNullable();
            table.integer("al_stat_system").notNullable();
            table.integer("al_stat_hits").notNullable();
            table.integer("al_stat_mempeak").notNullable();
            table.integer("al_stat_servtime").notNullable();

            table.timestamp('al_timestamp').notNullable().defaultTo(knex.fn.now());
        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([

        knex.schema.dropTable('accesslog'),
    ])

};
