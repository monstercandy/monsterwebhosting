
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.createTable('slowlog', function(table) {
            table.increments('sl_id').primary();

            table.uuid('sl_user_id');

            table.integer('sl_webhosting')
                 .references('sl_id')
                 .inTable('webhostings');

            table.string("sl_appserver").notNullable();

            table.string("sl_path").notNullable();
            table.string("sl_dump").notNullable();

            table.timestamp('sl_timestamp').notNullable().defaultTo(knex.fn.now());
        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([

        knex.schema.dropTable('slowlog'),
    ])

};
