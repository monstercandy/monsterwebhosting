
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.boolean('wh_firewall').notNullable().defaultTo(false);

        }),


         knex.schema.createTable('firewall', function(table) {
            table.increments('fw_id').primary();

            table.integer('fw_webhosting')
                 .references('wh_id')
                 .inTable('webhostings');

            table.string("fw_dst_ip").notNullable();

            table.integer("fw_dst_port").notNullable();

            table.string("fw_action").notNullable().defaultTo("ACCEPT");

        }),

    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_firewall");
        }),
        knex.schema.dropTable('firewall'),        
    ])
    
};
