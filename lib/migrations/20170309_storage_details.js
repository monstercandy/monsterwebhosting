
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.json('wh_tally_web_storage').notNullable().defaultTo("{}");
            table.json('wh_tally_mail_storage').notNullable().defaultTo("{}");
            table.json('wh_tally_db_storage').notNullable().defaultTo("{}");

        }),


    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_tally_web_storage");
            table.dropColumn("wh_tally_mail_storage");
            table.dropColumn("wh_tally_db_storage");
        }),      
    ])
    
};
