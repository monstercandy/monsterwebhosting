
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostings', function(table) {
            table.integer('wh_max_execution_second').notNullable().defaultTo(0);
            table.string('wh_php_fpm_conf_extra_lines').notNullable().defaultTo("");

            table.decimal('wh_tally_sum_storage_mb').notNullable().defaultTo(0);

        }),


    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostings', function(table){
            table.dropColumn("wh_max_execution_second");
            table.dropColumn("wh_php_fpm_conf_extra_lines");
            table.dropColumn("wh_tally_sum_storage_mb");
        }),
    ])
    
};
