var lib = module.exports = function(app, webhosting){

	 if(!webhosting) webhosting = {}

    const dotq = require("MonsterDotq");
    const fs = dotq.fs();
    const path = require("path")

     var php_distribution_pathes = app.config.get("php_distributions") || []

            function getPhpFpmPathes(p) {
              if(!p.php_version) throw new Error("php_version was not set")
              if(!p.basedir) throw new Error("basedir was not set")


               var re = {
                  pid: p.pid_file || path.join(p.basedir, "logs", "php-fpm.pid"),
                  pools_directory: p.pools_directory || path.join(p.basedir, "etc", "pools")
               }

               if(!p.conf_file) {
                  re.config_directory = path.join(re.pools_directory, ""+webhosting.wh_id)
                  re.config =  path.join(re.config_directory, webhosting.wh_id+".conf")
               } else {
                  re.config = p.conf_file
               }

               return re

            }


            var re = {}

            function sendSignal(signal, version_filter, ignoreErrors) {


                const process = require("process")

                var rehashed = 0
                var work = {array:php_distribution_pathes, catch: true, action: p=>{
                      if((version_filter)&&(version_filter != p.php_version)) return

                      return fs.readFileAsync(getPhpFpmPathes(p).pid)
                        .then(pid =>{
                            process.kill(pid, signal)
                        })
                        .then(()=>{
                            rehashed++
                        })

                }};
                if(ignoreErrors)
                   work.catch = true;
                return dotq.linearMap(work)
                .then(()=>{
                   return rehashed;
                })
            }

            re.ReopenLogPhpFpm = function(version_filter){
               // kill -USR1 `cat /usr/local/php-7.0.9/logs/php-fpm.pid`
                return sendSignal("SIGUSR1", version_filter);
            }


            re.RehashPhpFpm = function(version_filter, ignoreErrors){
               // kill -USR2 `cat /usr/local/php-7.0.9/logs/php-fpm.pid`

               return sendSignal("SIGUSR2", version_filter, ignoreErrors);

            }


            re.RemovePhpFpmConfig = function(){
            	if(!webhosting.wh_id) throw new Error("webhosting object required")

              var ps = []
              php_distribution_pathes.forEach(p=>{

                   var pathes = getPhpFpmPathes(p)

                    ps.push(
                         fs.unlinkAsync(pathes.config)
                            .catch(ex=>{
                                console.error("Unable to remove PHP-FPM config file:", ex)
                            })
                            .then(()=>{

                                if(!pathes.config_directory) return Promise.resolve()

                                return fs.rmdirAsync(pathes.config_directory).catch(ex=>{})

                            })
                      )
              })
              return Promise.all(ps)

            }

            re.ReadPhpFpmConfig = function(){
               var re = {}
               var ps = []
               php_distribution_pathes.forEach(dist=>{
                    var pathes = getPhpFpmPathes(dist)
                    ps.push(
                      fs.readFileAsync(pathes.config,"utf8")
                        .then(data=>{
                            re[dist.basedir] = data
                        })
                    )
               })

               return Promise.all(ps)
                 .then(()=>{
                    return Promise.resolve(re)
                 })
            }

            re.SavePhpFpmConfig = function(){
            	if(!webhosting.wh_id) throw new Error("webhosting object requeired")

               if(php_distribution_pathes.length <= 0) return Promise.resolve()

               webhosting.php_fpm_execution_time = `request_terminate_timeout = 120s\n`
               if(webhosting.wh_max_execution_second) {
                  webhosting.php_fpm_execution_time = `request_terminate_timeout = ${webhosting.wh_max_execution_second}s\n`
                  webhosting.php_fpm_execution_time += `php_value[max_execution_time] = ${webhosting.wh_max_execution_second}\n`
               }

               return fs.readFileAsync(app.config.get("php_fpm_template_path"), "utf8")
                 .then(template=>{

                      const phpLib = require("MonsterPhp")
                      const replacer = require("Replacer")

                      var ps = []
                      php_distribution_pathes.forEach(p=>{

                            webhosting.php_basedir = p.basedir;
                            var php_hostport = phpLib.phpport(p.php_version, webhosting.wh_id, app.config.get("php_ip_model"));
                            if(php_hostport) {
                              webhosting.php_port = php_hostport.port;
                              webhosting.php_host = php_hostport.host;                              
                            }
                            webhosting.php_request_uri = p.php_version > "5.4" ? " %{REQUEST_URI}e" : "";
                            webhosting.php_fpm_accesslog_disabled = app.config.get("php_fpm_accesslog_disabled") ? "#" : "";
                            webhosting.php_logdir = p.php_logdir || path.join(p.basedir, "logs")

                            var conf = replacer(template, webhosting);


                            var pathes = getPhpFpmPathes(p)

                            var p
                            if(pathes.config_directory)
                              p = fs.mkdirAsync(pathes.config_directory).catch(ex=>{})
                            else
                              p = Promise.resolve()


                            ps.push(p.then(()=>{
                               return fs.writeFileAsync(pathes.config, conf)
                            }))
                      })
                      return Promise.all(ps)


                 })


            }


            return re
}
