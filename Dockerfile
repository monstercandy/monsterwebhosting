FROM monstercommon

ADD lib /opt/MonsterWebhosting/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterWebhosting/lib/bin/webhosting-install.sh && /opt/MonsterWebhosting/lib/bin/webhosting-test.sh

ENTRYPOINT ["/opt/MonsterWebhosting/lib/bin/webhosting-start.sh"]
